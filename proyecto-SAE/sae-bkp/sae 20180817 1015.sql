-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.13-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema sae
--

CREATE DATABASE IF NOT EXISTS sae;
USE sae;

--
-- Definition of table `comisiones`
--

DROP TABLE IF EXISTS `comisiones`;
CREATE TABLE `comisiones` (
  `COM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PER_ID` int(10) unsigned NOT NULL,
  `COM_TURNO` varchar(45) NOT NULL,
  `COM_DIVISION` varchar(45) NOT NULL,
  PRIMARY KEY (`COM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comisiones`
--

/*!40000 ALTER TABLE `comisiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `comisiones` ENABLE KEYS */;


--
-- Definition of table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE `cursos` (
  `CUR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ANIO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CUR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cursos`
--

/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;


--
-- Definition of table `evaluaciones`
--

DROP TABLE IF EXISTS `evaluaciones`;
CREATE TABLE `evaluaciones` (
  `EVAL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MAT_ID` int(10) unsigned NOT NULL,
  `PER_ID` int(10) unsigned NOT NULL,
  `EVAL_ESTADO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`EVAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluaciones`
--

/*!40000 ALTER TABLE `evaluaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluaciones` ENABLE KEYS */;


--
-- Definition of table `itemsdetemario`
--

DROP TABLE IF EXISTS `itemsdetemario`;
CREATE TABLE `itemsdetemario` (
  `ITEM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEM_ID` int(10) unsigned NOT NULL,
  `ITEM_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemsdetemario`
--

/*!40000 ALTER TABLE `itemsdetemario` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsdetemario` ENABLE KEYS */;


--
-- Definition of table `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `MAT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ID` int(10) unsigned NOT NULL,
  `MAT_NOMBRE` varchar(45) NOT NULL,
  `TEM_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`MAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materias`
--

/*!40000 ALTER TABLE `materias` DISABLE KEYS */;
/*!40000 ALTER TABLE `materias` ENABLE KEYS */;


--
-- Definition of table `modulos`
--

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos` (
  `MOD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MAT_ID` int(10) unsigned NOT NULL,
  `MOD_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`MOD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modulos`
--

/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;


--
-- Definition of table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
CREATE TABLE `partidos` (
  `PART_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_ID` int(10) unsigned NOT NULL DEFAULT '1',
  `PART_NOMBRE` varchar(45) NOT NULL,
  PRIMARY KEY (`PART_ID`) USING BTREE,
  KEY `FK_partidos_provincia` (`PROV_ID`),
  CONSTRAINT `FK_partidos_provincia` FOREIGN KEY (`PROV_ID`) REFERENCES `provincias` (`PROV_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partidos`
--

/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` (`PART_ID`,`PROV_ID`,`PART_NOMBRE`) VALUES 
 (5,1,'Adolfo Alsina'),
 (6,1,'Adolfo Gonzales Chaves'),
 (7,1,'Alberti'),
 (8,1,'Almirante Brown'),
 (9,1,'Arrecifes'),
 (10,1,'Avellaneda'),
 (11,1,'Ayacucho'),
 (12,1,'Azul'),
 (13,1,'Bahía Blanca'),
 (14,1,'Balcarce'),
 (15,1,'Baradero'),
 (16,1,'Benito Juárez'),
 (17,1,'Berazategui'),
 (18,1,'Berisso'),
 (19,1,'Bolívar'),
 (20,1,'Bragado'),
 (21,1,'Brandsen'),
 (22,1,'Campana'),
 (23,1,'Cañuelas'),
 (24,1,'Capitán Sarmiento Carlos'),
 (25,1,'Carlos Casares'),
 (26,1,'Carlos Tejedor'),
 (27,1,'Carmen de Areco'),
 (28,1,'Castelli'),
 (29,1,'Chacabuco'),
 (30,1,'Chascomús'),
 (31,1,'Chivilcoy'),
 (32,1,'Colón'),
 (33,1,'Coronel de Marina Leonardo Rosales'),
 (34,1,'Coronel Dorrego'),
 (35,1,'Coronel Pringles'),
 (36,1,'Coronel Suárez'),
 (37,1,'Daireaux'),
 (38,1,'Dolores'),
 (39,1,'Ensenada'),
 (40,1,'Escobar'),
 (41,1,'Esteban Echeverría'),
 (42,1,'Exaltación de la Cruz'),
 (43,1,'Ezeiza'),
 (44,1,'Florencio Varela'),
 (45,1,'Florentino Ameghino'),
 (46,1,'General Alvarado'),
 (47,1,'General Alvear'),
 (48,1,'General Arenales'),
 (49,1,'General Belgrano'),
 (50,1,'General Guido'),
 (51,1,'General Juan Madariaga'),
 (52,1,'General La Madrid'),
 (53,1,'General Las Heras'),
 (54,1,'General Lavalle'),
 (55,1,'General Paz'),
 (56,1,'General Pinto'),
 (57,1,'General Pueyrredón'),
 (58,1,'General Rodríguez'),
 (59,1,'General San Martín'),
 (60,1,'General Viamonte'),
 (61,1,'General Villegas'),
 (62,1,'Guaminí'),
 (63,1,'Hipólito Yrigoyen'),
 (64,1,'Hurlingham'),
 (65,1,'Ituzaingó'),
 (66,1,'José C. Paz'),
 (67,1,'Junín'),
 (68,1,'La Costa'),
 (69,1,'La Matanza'),
 (70,1,'Lanús'),
 (71,1,'La Plata'),
 (72,1,'Laprida'),
 (73,1,'Las Flores'),
 (74,1,'Leandro N. Alem'),
 (75,1,'Lincoln'),
 (76,1,'Lobería'),
 (77,1,'Lobos'),
 (78,1,'Lomas de Zamora'),
 (79,1,'Luján'),
 (80,1,'Magdalena'),
 (81,1,'Maipú'),
 (82,1,'Malvinas Argentinas'),
 (83,1,'Mar Chiquita'),
 (84,1,'Marcos Paz'),
 (85,1,'Mercedes'),
 (86,1,'Merlo'),
 (87,1,'Monte'),
 (88,1,'Monte Hermoso'),
 (89,1,'Moreno'),
 (90,1,'Morón'),
 (91,1,'Navarro'),
 (92,1,'Necochea'),
 (93,1,'Nueve de Julio (9 de Julio)'),
 (94,1,'Olavarría'),
 (95,1,'Patagones'),
 (96,1,'Pehuajó'),
 (97,1,'Pellegrini'),
 (98,1,'Pergamino'),
 (99,1,'Pila'),
 (100,1,'Pilar'),
 (101,1,'Pinamar'),
 (102,1,'Presidente Perón'),
 (103,1,'Puan'),
 (104,1,'Punta Indio'),
 (105,1,'Quilmes'),
 (106,1,'Ramallo'),
 (107,1,'Rauch'),
 (108,1,'Rivadavia'),
 (109,1,'Rojas'),
 (110,1,'Roque Pérez'),
 (111,1,'Saavedra'),
 (112,1,'Saladillo'),
 (113,1,'Salliqueló'),
 (114,1,'Salto'),
 (115,1,'San Andrés de Giles'),
 (116,1,'San Antonio de Areco'),
 (117,1,'San Cayetano'),
 (118,1,'San Fernando'),
 (119,1,'San Isidro'),
 (120,1,'San Miguel'),
 (121,1,'San Nicolás'),
 (122,1,'San Pedro'),
 (123,1,'San Vicente'),
 (124,1,'Suipacha'),
 (125,1,'Tandil'),
 (126,1,'Tapalqué'),
 (127,1,'Tigre'),
 (128,1,'Tordillo'),
 (129,1,'Tornquist'),
 (130,1,'Trenque Lauquen'),
 (131,1,'Tres Arroyos'),
 (132,1,'Tres de Febrero'),
 (133,1,'Tres Lomas'),
 (134,1,'Veinticinco de Mayo (25 de Mayo)'),
 (135,1,'Vicente López'),
 (136,1,'Villa Gesell'),
 (137,1,'Villarino'),
 (138,1,'Zárate');
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;


--
-- Definition of table `personas`
--

DROP TABLE IF EXISTS `personas`;
CREATE TABLE `personas` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PART_ID` int(10) unsigned DEFAULT NULL,
  `PROV_ID` int(10) unsigned DEFAULT NULL,
  `COM_ID` int(10) unsigned DEFAULT NULL,
  `PER_NOMBRE` varchar(45) NOT NULL,
  `PER_APELLIDO` varchar(45) NOT NULL,
  `PER_DIRECCION` varchar(45) DEFAULT NULL,
  `PER_TELEFONO` varchar(45) DEFAULT NULL,
  `PER_DNI` varchar(45) DEFAULT NULL,
  `PER_EMAIL` varchar(45) DEFAULT NULL,
  `PER_TIPOPERSONA` varchar(45) NOT NULL,
  `PER_IOSFA` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personas`
--

/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;


--
-- Definition of table `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
CREATE TABLE `preguntas` (
  `PREG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EVAL_ID` int(10) unsigned NOT NULL,
  `MOD_ID` int(10) unsigned NOT NULL,
  `MAT_ID` int(10) unsigned NOT NULL,
  `PREG_PATHGRAFICO` varchar(45) NOT NULL,
  `PREG_EXPLICACION` varchar(45) NOT NULL,
  `PREG_TEXTO` varchar(45) NOT NULL,
  PRIMARY KEY (`PREG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preguntas`
--

/*!40000 ALTER TABLE `preguntas` DISABLE KEYS */;
/*!40000 ALTER TABLE `preguntas` ENABLE KEYS */;


--
-- Definition of table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE `provincias` (
  `PROV_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_NOMBRE` varchar(100) NOT NULL,
  PRIMARY KEY (`PROV_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provincias`
--

/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` (`PROV_ID`,`PROV_NOMBRE`) VALUES 
 (1,'Buenos Aires'),
 (40,'Catamarca'),
 (41,'Chaco'),
 (42,'Chubut'),
 (43,'Córdoba'),
 (44,'Corrientes'),
 (45,'Entre Ríos'),
 (46,'Formosa'),
 (47,'Jujuy'),
 (48,'La Pampa'),
 (49,'La Rioja'),
 (50,'Mendoza'),
 (51,'Misiones'),
 (52,'Neuquén'),
 (53,'Río Negro'),
 (54,'Salta	SA'),
 (55,'San Juan'),
 (56,'San Luis'),
 (57,'Santa Cruz'),
 (58,'Santa Fe'),
 (59,'Santiago del Estero'),
 (60,'Tierra del Fuego, Antártida e Islas del Atlántico Sur'),
 (61,'Tucumán');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;


--
-- Definition of table `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE `respuestas` (
  `RES_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PREG_ID` int(10) unsigned NOT NULL,
  `RES_TEXTO` varchar(45) NOT NULL,
  `RES_ISCORRECTA` tinyint(1) NOT NULL,
  `RES_ISSELECCIONADA` tinyint(1) NOT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `respuestas`
--

/*!40000 ALTER TABLE `respuestas` DISABLE KEYS */;
/*!40000 ALTER TABLE `respuestas` ENABLE KEYS */;


--
-- Definition of table `temarios`
--

DROP TABLE IF EXISTS `temarios`;
CREATE TABLE `temarios` (
  `TEM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEM_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temarios`
--

/*!40000 ALTER TABLE `temarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `temarios` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
