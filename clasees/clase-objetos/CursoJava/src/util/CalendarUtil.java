package util;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Gabriel
 * Esta clase tiene la finalidad de ofrecer un conjunto de metodos
 * que trabajan  con fechas
 */
public class CalendarUtil {
	/**
	 * Este metodo permite devolver el a�o de la fecha recibida
	 * @param pFecha corresponda a la fecha  recibida
	 * @return es el a�o de la fecha
	 */
	public static int getAnio(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.YEAR);
	}
	public static int getMes(Date pFecha){
		//TODO Carlitos cuando ingrese
		return 0;
		
	}
	public static int getDia(Date pFecha){
		//TODO Matias fijate como haces to
		return 0;
	}
	
	public static boolean isFinDeSeman(Date pFecha){
		boolean blnResult=false;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		//necesito el dia de la seman
		int diaDeLaSemana = cal.get(Calendar.DAY_OF_WEEK);
		if(diaDeLaSemana==Calendar.SATURDAY || diaDeLaSemana==Calendar.SUNDAY)
			blnResult=true;		
		return blnResult;
	}
	public static Date asDate(String pFecha) {
		//17/11/1972
		//0123456789
		int dia = Integer.parseInt(pFecha.substring(0,2));
		int mes = Integer.parseInt(pFecha.substring(3,5));
		int anio= Integer.parseInt(pFecha.substring(6));
		
		Calendar cal = Calendar.getInstance();
		cal.set(anio,mes-1, dia);
		return cal.getTime();
	}

	

}
