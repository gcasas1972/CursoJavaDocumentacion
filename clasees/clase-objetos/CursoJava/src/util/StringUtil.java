package util;

public class StringUtil {
	public static boolean containsDobleSpace(String pStr){
		return pStr.contains("  ");
		
	}
	
	public static boolean containsNumber(String pStr){
		for(int i =0;i<pStr.length();i++){
			if(Character.isDigit(pStr.charAt(i)))
					return true;
		}
		return false;
	}

}
