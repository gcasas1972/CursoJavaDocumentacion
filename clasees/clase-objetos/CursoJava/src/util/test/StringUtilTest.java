package util.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import util.StringUtil;

public class StringUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testContainsDobleSpaceTrue() {
		String str= new String("Hola  Gabrielito");
		assertTrue(StringUtil.containsDobleSpace(str));		
	}
	@Test
	public void testContainsDobleSpaceFalse() {
		String str= new String("Hola Gabrielito");
		assertFalse(StringUtil.containsDobleSpace(str));		
	}
	
	@Test
	public void testContainsNumberTrue() {
		String str=new String("Juan123");
		assertTrue(StringUtil.containsNumber(str));
	}

	@Test
	public void testContainsNumberFalse() {
		String str=new String("Juan");
		assertFalse(StringUtil.containsNumber(str));
	}
	
}
