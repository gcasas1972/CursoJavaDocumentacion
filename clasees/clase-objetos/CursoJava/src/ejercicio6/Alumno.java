package ejercicio6;

import util.StringUtil;

public class Alumno {
	private String codigo;
	private String nombre;
	private String telefono;
	private String email;
	private String fechaDeNacimiento;
	
	public Alumno(){}
	public Alumno(	String pCodigo, 
					String pNombre, 
					String pTelefono, 
					String pEmail	,
					String pFechaDeNacimiento){
		
		codigo 				= pCodigo;
		nombre 				= pNombre;
		telefono 			= pTelefono;
		email				= pEmail;
		fechaDeNacimiento 	= pFechaDeNacimiento;
				
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String pNombre) {
		//hago la comparacion solicitada
		if(StringUtil.containsDobleSpace(pNombre) ||
		   StringUtil.containsNumber(pNombre)	)
			nombre="";
		else
			nombre = pNombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String pEmail) {
		/*verifico que
		 * 1- si no exista @
		 * 2- si esta al principio
		 * 3- si esta al final
		 * 4- Si contiene un espacio
		 * */ 
		if(pEmail.indexOf("@") == -1 ||
		   pEmail.indexOf("@") == 0  ||
		   pEmail.indexOf("@") == pEmail.length()-1 ||
		   pEmail.contains(" "))
				email ="";
		else
				email = pEmail;
	}
	
	public String getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(String fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	
	
}
