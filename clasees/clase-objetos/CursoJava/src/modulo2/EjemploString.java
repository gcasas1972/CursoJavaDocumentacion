package modulo2;

public class EjemploString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = new String("Hola Gabrielito");
		
		System.out.println("s1 original=" + s1);
		System.out.println("s1 Mayuscula=" + s1.toUpperCase());
		System.out.println("s1 Minuscula=" + s1.toLowerCase());
		
	}

}
