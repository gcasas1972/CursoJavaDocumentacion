package modulo1;

public class EjemploContador {

	/**
	 * Este es un programa que genere numeros al azar y determina
	 * la cantidad de numeros pares 
	 * @param args
	 */
	public static void main(String[] args) {
		
		int pares	=0;
		int num		=0;
		int resto	=0;
		//docenas
		int docena1	=0;
		int docena2	=0;
		int docena3	=0;
		int fuera	=0;
		for(int i =0;i<10;i++){
			num =(int) (Math.random()*100%100);
			System.out.println(i + " numero =" + num);
			resto = num%2;
			if(resto==0)
				pares++;
			if(num>0 && num<13)
				docena1++;
			else if(num<24)
				docena2++;
			else if(num<37)
				docena3++;
			else 
				fuera ++;		
		}
		System.out.println("la cantidad de numeros pares es " 	+ pares		);
		System.out.println("la cantidad de primer docena " 		+ docena1	);
		System.out.println("la cantidad de segunda docena "		+ docena2	);
		System.out.println("la cantidad de tercer docena " 		+ docena3	);
		System.out.println("la cantidad de fuera de rango "		+ fuera		);
	}

}
