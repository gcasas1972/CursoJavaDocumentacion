package ar.com.utn.bolsaDeTrabajo.model;

import java.util.Date;
import java.util.List;

public class Empresa {
	
	//Atributos
	private List contactos;
	private Convenio convenioAlumno;
	private Convenio convenioFacultad;
	private int cuit_codigoVerificador;
	private int cuit_dosDigitos;
	private String domicilioLegal;
	private String fax;
	private Date fechaDeActualizacion;
	private Rubro rubro;
	private String zonaDeTrabajo;
	
	//Constructor
	public Empresa(){
		
	}
	//Constructor con paramtros
	public Empresa(List contactos, int cuit_codigoVerificador,
			int cuit_dosDigitos, String domicilioLegal, String fax,
			Date fechaDeActualizacion, String zonaDeTrabajo) {
		super();
		this.contactos = contactos;
		this.cuit_codigoVerificador = cuit_codigoVerificador;
		this.cuit_dosDigitos = cuit_dosDigitos;
		this.domicilioLegal = domicilioLegal;
		this.fax = fax;
		this.fechaDeActualizacion = fechaDeActualizacion;
		this.zonaDeTrabajo = zonaDeTrabajo;
	}

	//getter Contactos
	public List getContactos() {
		return contactos;
	}

	//setter Contactos
	public void setContactos(List contactos) {
		this.contactos = contactos;
	}

	//getter Cuit_codigoVerificador
	public int getCuit_codigoVerificador() {
		return cuit_codigoVerificador;
	}

	//setter Cuit_codigoVerificador
	public void setCuit_codigoVerificador(int cuit_codigoVerificador) {
		this.cuit_codigoVerificador = cuit_codigoVerificador;
	}

	//getter Cuit_dosDigitos
	public int getCuit_dosDigitos() {
		return cuit_dosDigitos;
	}

	//setter Cuit_dosDigitos
	public void setCuit_dosDigitos(int cuit_dosDigitos) {
		this.cuit_dosDigitos = cuit_dosDigitos;
	}

	//getter DomicilioLegal
	public String getDomicilioLegal() {
		return domicilioLegal;
	}

	//setter DomicilioLegal
	public void setDomicilioLegal(String domicilioLegal) {
		this.domicilioLegal = domicilioLegal;
	}

	//getter Fax
	public String getFax() {
		return fax;
	}

	//setter Fax
	public void setFax(String fax) {
		this.fax = fax;
	}

	//getter FechaDeActualizacion
	public Date getFechaDeActualizacion() {
		return fechaDeActualizacion;
	}

	//setter FechaDeActualizacion
	public void setFechaDeActualizacion(Date fechaDeActualizacion) {
		this.fechaDeActualizacion = fechaDeActualizacion;
	}

	//getter ZonaDeTrabajo
	public String getZonaDeTrabajo() {
		return zonaDeTrabajo;
	}

	//setter ZonaDeTrabajo
	public void setZonaDeTrabajo(String zonaDeTrabajo) {
		this.zonaDeTrabajo = zonaDeTrabajo;
	}
	
	//hashCode
	
	@Override
	public int hashCode() {

		return super.hashCode();
	}
	
	//equals
	
	@Override
	public boolean equals(Object obj) {
		//agustin sos trucho usastes el automatico
		boolean bln=false;
		Empresa emp=null;
		if (obj instanceof Empresa){
			emp = (Empresa) obj;
			bln = super.equals(emp);
		}
		
		return bln;	
	}
	
	//toString
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer(super.toString());
		//El StringBuffer es MUTABLE y mas RAPIDO
		
		sb.append(contactos); //APPEND es sumar
		sb.append("contactos=");
		
		sb.append(cuit_codigoVerificador);
		sb.append("cuit_codigoVerificador=");
		
		sb.append(cuit_dosDigitos);
		sb.append("cuit_dosDigitos=");
		
		sb.append(domicilioLegal);
		sb.append("domicilioLegal=");
		
		sb.append(fax);
		sb.append("fax=");
		
		sb.append(fechaDeActualizacion);
		sb.append("fechaDeActualizacion=");
		
		sb.append(zonaDeTrabajo);
		sb.append("zonaDeTrabajo");
		
		return sb.toString();

	}
	

}
