DROP TABLE IF EXISTS `oferlab`.`conocimientosdeidiomas`;
CREATE TABLE  `oferlab`.`conocimientosdeidiomas` (
  `CONIDI_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CONIDI_NIVELDECONOCIMIENTO` int(10) unsigned NOT NULL,
  `IDIO_CODIGO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CONIDI_CODIGO`),
  KEY `FK_conocimientosdeidiomas_idioma` (`IDIO_CODIGO`),
  CONSTRAINT `FK_conocimientosdeidiomas_idioma` FOREIGN KEY (`IDIO_CODIGO`) REFERENCES `idiomas` (`IDIO_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;