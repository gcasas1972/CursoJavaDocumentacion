package ar.com.utn.bolsaDeTrabajo.model;

/**
 * @author Daniela
 * Esta clase indica el estado civil del alumno aplicante a un posible trabajo.
 */

public class EstadoCivil {

/**
 * @param codigo,  indica el c�digo que corresponde a cada estado civil
 * @return int, el n�mero de c�digo que indica el estado civil del alumno en cuesti�n
 * @param descripci�n, indica el estado civil como figura en su registro civil.
 * @return String, la descripci�n del estado civil en palabras. EJ: Soltero/a, Casado/a, etc.
 */
	
	
	//constructores
	public EstadoCivil (){}
	public EstadoCivil (int pcodigo, String pdescripcion){
		codigo=pcodigo;
		descripcion = pdescripcion;
	}

	//atributos
	private int 		codigo;		
	private String		descripcion;
	
	//getters y setters
	public int getCodigo(){		return codigo;	}
	public void setCodigo(int pcodigo){ codigo = pcodigo;}
			
	public String getDescripcion(){		return descripcion;	}
	public void setDescripcion(String pdescripcion){ descripcion = pdescripcion;}

	//Equals
	public boolean equals(Object obj){
		boolean bln=false;
		EstadoCivil estadocivil = null;
		
		if(obj instanceof EstadoCivil){
			estadocivil=(EstadoCivil)obj;
			//codigo= estadocivil.getCodigo();
			bln= codigo == estadocivil.getCodigo();
		}
		return bln;
	}
	
	//Hashcode
	@Override
	public int hashCode() {
		return codigo+descripcion.hashCode();
	}
	
	//ToString
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C�digo = ");
		sb.append(codigo);
		sb.append("Descripci�n = ");
		sb.append(descripcion);
		
		
		return sb.toString();
	}
}
