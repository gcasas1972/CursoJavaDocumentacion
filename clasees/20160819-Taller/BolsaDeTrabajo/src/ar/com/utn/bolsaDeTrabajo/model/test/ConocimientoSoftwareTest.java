package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.ConocimientoSoftware;


public class ConocimientoSoftwareTest {
	
	public static ConocimientoSoftware conocimientosoftware;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		conocimientosoftware = new ConocimientoSoftware();
		conocimientosoftware.setCodigo(15);
		conocimientosoftware.setSoftware(null);
		conocimientosoftware.setNivelDeConocimiento(10);
	}
	
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(15, conocimientosoftware.getCodigo());
	}
		
	@Test
	public void setSoftwareTest() {
		assertEquals(null, conocimientosoftware.getSoftware());
	}
	
	@Test
	public void setNivelDeConocimientoTest() {
		assertEquals(10, conocimientosoftware.getNivelDeConocimiento());
	}

	@Test
	public void equalsTest_correcto() {
		ConocimientoSoftware conocimientosoftware1 = new ConocimientoSoftware(15, null, 10);
		Assert.assertTrue(conocimientosoftware.equals(conocimientosoftware1));
	}
	@Test
	public void equalsTest_falso() {
		ConocimientoSoftware conocimientosoftware2 = new ConocimientoSoftware(3, null, 5);
		Assert.assertFalse(conocimientosoftware.equals(conocimientosoftware2));
	}

}
