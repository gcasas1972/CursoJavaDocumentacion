package ar.com.utn.bolsaDeTrabajo.model.dao;

import java.sql.SQLException;
import java.util.List;

import ar.com.utn.bolsaDeTrabajo.model.exceptions.ModeloException;

public interface DAO {
	public boolean agregar(Object obj) throws SQLException, ModeloException;
	public boolean eliminar(Object obj)throws SQLException, ModeloException;
	public boolean modificar(Object obj)throws SQLException, ModeloException;
	public List leer(Object obj)throws SQLException, ModeloException;
	

}
