DROP TABLE IF EXISTS `oferlab`.`rubros`;
CREATE TABLE  `oferlab`.`rubros` (
  `RUB_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RUB_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`RUB_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;