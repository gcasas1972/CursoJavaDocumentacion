package ar.com.utn.bolsaDeTrabajo.model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.utn.bolsaDeTrabajo.model.Alumno;
import ar.com.utn.bolsaDeTrabajo.model.exceptions.ModeloException;
import ar.com.utn.bolsaDeTrabajo.util.ConnectionManager;

public class AlumnoDao implements DAO{
	Connection con;
	public AlumnoDao(Connection pCon){
		con=pCon;
		
	}

	@Override
	public boolean agregar(Object obj) throws SQLException, ModeloException {
		int iCant=0;
		String sql=null;
		Alumno alu = (Alumno)obj;
		if(alu.isEmpty())
			throw new ModeloException("El objeto alumno esta vacio");
		sql =	"insert into alumnos(alu_nombre, alu_apellido)" +
				"values('"+ alu.getNombre() + "'," +
					   "'"+ alu.getApellido()+ "')";
		Statement stm = con.createStatement();
		iCant=stm.executeUpdate(sql);
		
		
		return iCant>0;
	}

	@Override
	public boolean eliminar(Object obj) throws SQLException, ModeloException {
		int iCant=0;
		String sql=null;
		Alumno alu = (Alumno)obj;
		if(alu.isEmpty())
			throw new ModeloException("El alumno esta vacio");
		
		sql =	"delete from alumnos where alu_codigo=" + alu.getCodigo();		
		Statement stm = con.createStatement();
		iCant=stm.executeUpdate(sql);
		
		return iCant>0;
	}

	@Override
	public boolean modificar(Object obj) throws SQLException, ModeloException {
		int iCant=0;
		String sql=null;
		Alumno alu = (Alumno)obj;
		if(!alu.isModificable())
			throw new ModeloException("El alumno no tiene valores para modificar o no tiene codigo");
		
		sql =	"update alumnos set  alu_nombre ='"+ alu.getNombre() + "'," 	+
								" alu_apellido='"+ alu.getApellido()+ "'" 		+
					" where alu_codigo=" + alu.getCodigo();
					
		Statement stm = con.createStatement();
		iCant=stm.executeUpdate(sql);

		return iCant>0;
	}

	/* (non-Javadoc)
	 * @see ar.com.utn.bolsaDeTrabajo.model.dao.DAO#leer(java.lang.Object)
	 */
	@Override
	public List leer(Object obj) throws SQLException, ModeloException {
		ResultSet rs=null;
		int iCant=0;
		String sql=null;
		Alumno alu = (Alumno)obj;
		if(alu.isEmpty())
			throw new ModeloException("El alumno esta vacio");
		
		sql =	"select  alu_codigo,alu_nombre, alu_apellido from alumnos " +
					"where alu_nombre='" + alu.getNombre() + "'";
					
		Statement stm = con.createStatement();
		rs=stm.executeQuery(sql);
		
		return asAlumnos(rs);
	}

	private List asAlumnos(ResultSet rs) throws SQLException {
		List alumnos = new ArrayList();
		while(rs.next()){
			alumnos.add(new Alumno(rs.getInt("alu_codigo"), rs.getString("alu_nombre"), rs.getString("alu_apellido")));
		}
		Iterator<Alumno> iter;
		
		return alumnos;
	}

	
}
