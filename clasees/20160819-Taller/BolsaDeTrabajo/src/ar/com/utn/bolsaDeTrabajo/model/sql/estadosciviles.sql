DROP TABLE IF EXISTS `oferlab`.`estadosciviles`;
CREATE TABLE  `oferlab`.`estadosciviles` (
  `ESTC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ESTC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`ESTC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;