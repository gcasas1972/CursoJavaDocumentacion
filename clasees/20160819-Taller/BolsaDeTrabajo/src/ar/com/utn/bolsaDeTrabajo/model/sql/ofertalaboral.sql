DROP TABLE IF EXISTS `oferlab`.`ofertalaboral`;
CREATE TABLE  `oferlab`.`ofertalaboral` (
  `OFER_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CAR_CODIGO` int(10) unsigned NOT NULL,
  `CONT_CODIGO` int(10) unsigned NOT NULL,
  `EMP_CODIGO` int(10) unsigned NOT NULL,
  `OFER_ESTADO` int(10) unsigned NOT NULL,
  `JOR_CODIGO` int(10) unsigned NOT NULL,
  `OFER_REFERENCIA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`OFER_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;