package ar.com.utn.bolsaDeTrabajo.model.sqlModeloFactory;

import java.util.List;

public abstract class SqlModeloFactory {
	private List<SqlModeloFactory> objetosModelo;
	protected Object objetoDelModelo;
	
	public static Object getInstance(Object obj){
		
		return null;
	}
	public abstract String getCamposDeWhere();
	public abstract String getCamposDeSelect();
	public abstract String getCamposDeUpdate();
	public abstract boolean validar();
}
