package ar.com.utn.bolsaDeTrabajo.model;

public class Partido {

	public Partido(){}
	public Partido(int codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	private int codigo;
	private String descripcion;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public boolean equals(Object obj){
		boolean bln=false;
		Partido partido= null;		
		if(obj instanceof Partido){
			partido=(Partido) obj;
			bln =  partido.getCodigo()==codigo;
		}
		return bln;
	}
	@Override
	public int hashCode() {		
		return codigo;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(",descripcion=");
		sb.append(descripcion);		
		return sb.toString();
	}
}
	