package ar.com.utn.bolsaDeTrabajo.util.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.util.ConnectionManager;

public class ConnectionManagerTest {
	ConnectionManager cm = null;
	@Before
	public void setUp() throws Exception {
		cm = new ConnectionManager();
		cm.conectar();
		
	}


	@Test
	public void testConectar() {
		assertNotNull(cm.getConnection());
	}

}
