DROP TABLE IF EXISTS `oferlab`.`tiposdesecundarios`;
CREATE TABLE  `oferlab`.`tiposdesecundarios` (
  `TIPSEC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPSEC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPSEC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;