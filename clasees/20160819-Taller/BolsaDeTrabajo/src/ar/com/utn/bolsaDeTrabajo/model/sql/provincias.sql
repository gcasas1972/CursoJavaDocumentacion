DROP TABLE IF EXISTS `oferlab`.`provincias`;
CREATE TABLE  `oferlab`.`provincias` (
  `PROV_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`PROV_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;