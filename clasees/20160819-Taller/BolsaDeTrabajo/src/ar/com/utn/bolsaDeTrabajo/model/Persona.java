package ar.com.utn.bolsaDeTrabajo.model;

public abstract class Persona {
	
	private Ciudad ciudad;
	private int codigo;
	private String dto;
	private String email;
	private String nombre;
	private int numero;
	private Pais pais;
	private Partido partido;
	private String piso;
	private Provincia provincia;
	private String telefonoAlternativo;
	private String telefonoParticular;
	private String calle;
	
/*----------------------------------CONSTRUCTORES--------------------------------*/
	public Persona(){}
	
public Persona(Ciudad ciudad, int codigo, String dto, String email, String nombre, int numero, Pais pais,
			Partido partido, String piso, Provincia provincia, String telefonoAlternativo, String telefonoParticular,
			String calle) {
		
		this.ciudad = ciudad;
		this.codigo = codigo;
		this.dto = dto;
		this.email = email;
		this.nombre = nombre;
		this.numero = numero;
		this.pais = pais;
		this.partido = partido;
		this.piso = piso;
		this.provincia = provincia;
		this.telefonoAlternativo = telefonoAlternativo;
		this.telefonoParticular = telefonoParticular;
		this.calle = calle;
	}

/*----------------------------------GETTERS & SETTERS--------------------------------*/
	
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public Ciudad getCiudad() {
		return ciudad;
	}
	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDto() {
		return dto;
	}
	public void setDto(String dto) {
		this.dto = dto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public Partido getPartido() {
		return partido;
	}
	public void setPartido(Partido partido) {
		this.partido = partido;
	}
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public Provincia getProvincia() {
		return provincia;
	}
	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}
	public String getTelefonoAlternativo() {
		return telefonoAlternativo;
	}
	public void setTelefonoAlternativo(String telefonoAlternativo) {
		this.telefonoAlternativo = telefonoAlternativo;
	}
	public String getTelefonoParticular() {
		return telefonoParticular;
	}
	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}

	
/*----------------------------------EQUALS,HASHCODE,TOSTRING--------------------------------*/
	@Override
	public int hashCode() {
		return codigo;
	}

	@Override
	public boolean equals(Object obj) {
	//COMPARO SOLO EL ATRIBUTO CODIGO
		boolean bln = false;
		Persona per = null;
		if (obj instanceof Persona){
			//down cast
			per = (Persona)obj;
			bln = per.getCodigo()==this.codigo;
		}
			return bln;
	}
	
	@Override	
	public String toString() {
		StringBuffer sb =new StringBuffer(super.toString());
		
		sb.append("Ciudad=");
		sb.append(ciudad.toString());
		sb.append("Codigo=");
		sb.append(codigo);
		sb.append("dto=");
		sb.append(dto);
		sb.append("Email=");
		sb.append(email);
		sb.append("Nombre=");
		sb.append(nombre);
		sb.append("Numero=");
		sb.append(numero);
		sb.append("Pais=");
		sb.append(pais.toString());
		sb.append("Partido=");
		sb.append(partido.toString());
		sb.append("Piso=");
		sb.append(piso);
		sb.append("Provicncia=");
		sb.append(provincia.toString());
		sb.append("TelefonoAlternativo=");
		sb.append(telefonoAlternativo);
		sb.append("TelefonoParticular=");
		sb.append(telefonoParticular);
		sb.append("Calle=");
		sb.append(calle);
		
		return sb.toString();
	}	
	
}
