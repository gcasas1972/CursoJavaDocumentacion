package ar.com.utn.bolsaDeTrabajo.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.OfertaLaboral;

public class OfertaLaboralTest {

	public static OfertaLaboral ofertalaboral;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ofertalaboral = new OfertaLaboral();
		ofertalaboral.setReferencia(2);
	}

	@Test
	public void setReferenciaTest() {
		assertEquals(2, ofertalaboral.getReferencia());
	}

	@Test
	public void equals_correcctoTest() {
		OfertaLaboral ofertalaboral2 = new OfertaLaboral();
		ofertalaboral2.setReferencia(2);
		Assert.assertTrue(ofertalaboral.equals(ofertalaboral2));
	}

	@Test
	public void equals_falsoTest() {
		OfertaLaboral ofertalaboral2 = new OfertaLaboral();
		ofertalaboral2.setReferencia(3);
		Assert.assertFalse(ofertalaboral.equals(ofertalaboral2));

	}
}
