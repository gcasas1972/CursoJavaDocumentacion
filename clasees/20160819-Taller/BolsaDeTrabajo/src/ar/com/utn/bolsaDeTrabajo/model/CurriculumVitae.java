package ar.com.utn.bolsaDeTrabajo.model;
/**
 * @author Sebastian
 *	corresponde a la info del cv de las personas
 */

import java.util.List;



public class CurriculumVitae {
	public CurriculumVitae (){}
	
	
	public CurriculumVitae(int anioFinalizacionSecundario, int codigo,
			char confidencial, List conocimientosDeIdiomas,
			List conocimientosDeSoftware, List experienciaLaborales,
			String instituto,
			ar.com.utn.bolsaDeTrabajo.model.TipoDeSecundario tipoDeSecundario) {
		super();
		this.anioFinalizacionSecundario = anioFinalizacionSecundario;
		this.codigo = codigo;
		this.confidencial = confidencial;
		this.conocimientosDeIdiomas = conocimientosDeIdiomas;
		this.conocimientosDeSoftware = conocimientosDeSoftware;
		this.experienciaLaborales = experienciaLaborales;
		this.instituto = instituto;
		TipoDeSecundario = tipoDeSecundario;
	}


	private int anioFinalizacionSecundario;
	private int codigo;
	private char confidencial;
	private List conocimientosDeIdiomas;
	private List conocimientosDeSoftware;
	private List experienciaLaborales;
	private String instituto;
	private TipoDeSecundario TipoDeSecundario;
	public int getAnioFinalizacionSecundario() {
		return anioFinalizacionSecundario;
	}


	public void setAnioFinalizacionSecundario(int anioFinalizacionSecundario) {
		this.anioFinalizacionSecundario = anioFinalizacionSecundario;
	}


	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	public char getConfidencial() {
		return confidencial;
	}


	public void setConfidencial(char confidencial) {
		this.confidencial = confidencial;
	}


	public List getConocimientosDeIdiomas() {
		return conocimientosDeIdiomas;
	}


	public void setConocimientosDeIdiomas(List conocimientosDeIdiomas) {
		this.conocimientosDeIdiomas = conocimientosDeIdiomas;
	}


	public List getConocimientosDeSoftware() {
		return conocimientosDeSoftware;
	}


	public void setConocimientosDeSoftware(List conocimientosDeSoftware) {
		this.conocimientosDeSoftware = conocimientosDeSoftware;
	}


	public List getExperienciaLaborales() {
		return experienciaLaborales;
	}


	public void setExperienciaLaborales(List experienciaLaborales) {
		this.experienciaLaborales = experienciaLaborales;
	}


	public String getInstituto() {
		return instituto;
	}


	public void setInstituto(String instituto) {
		this.instituto = instituto;
	}


	public TipoDeSecundario getTipoDeSecundario() {
		return TipoDeSecundario;
	}


	public void setTipoDeSecundario(TipoDeSecundario tipoDeSecundario) {
		TipoDeSecundario = tipoDeSecundario;
	}
public boolean equals(Object obj) {
		
		boolean bln=false;
		CurriculumVitae per =null;
		if(obj instanceof CurriculumVitae){
		
			per=(CurriculumVitae) obj;
			bln= per.getCodigo()== codigo;
		}
		return bln;
	}		
@Override
public int hashCode(){
		return codigo;
}
@Override
	public String toString() {
		StringBuffer sb =new StringBuffer("codigo=");
		sb.append(codigo);
		
	return sb.toString();
		
	}
}
