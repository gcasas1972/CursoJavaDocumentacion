DROP TABLE IF EXISTS `oferlab`.`tiposdedocumentos`;
CREATE TABLE  `oferlab`.`tiposdedocumentos` (
  `TIPDOC_CODIGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPDOC_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TIPDOC_CODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;