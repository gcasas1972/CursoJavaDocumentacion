package ar.com.utn.bolsaDeTrabajo.model;

import java.util.Date;

public class Alumno extends Persona {
	  private Carrera carrera;
	 
	private String apellido; //VERIFICAR!!!!!!!!!
	
	private int cuil_DigitoVerificador;
	private int cuil_dni;
	private int cuil_DosDigitos;
	private CurriculumVitae curriculumVitae;
	private int estado_carrera;
	private EstadoCivil estadoCivil; 
	private Date fechaDeNacimiento;
	private String instituto;
	private int legajo_final;
	private int legajo_inicio;
	private String nacionalidad;
	private long numeroDeDocumento;
	private float promedioConAplazos;
	private char sexo;
	private char status;
	private TipoDeDocumento tipoDeDocumento;

/*----------------------------------CONSTRUCTORES--------------------------------*/
	public Alumno(){}
	
	public Alumno(String nombre,String email,String apellido,long numeroDeDocumento,Date fechaDeNacimiento, int legajo_inicio ){
		
		
		setApellido(apellido);
		setNumeroDeDocumento(numeroDeDocumento);
		setFechaDeNacimiento(fechaDeNacimiento);
		setLegajo_inicio(legajo_inicio);
	}
	
	public Alumno(Ciudad ciudad, int codigo, String dto, String email, String nombre, int numero, Pais pais,
			Partido partido, String piso, Provincia provincia, String telefonoAlternativo, String telefonoParticular,
			String calle, Carrera carrera, String apellido, int cuil_DigitoVerificador, int cuil_dni,
			int cuil_DosDigitos, CurriculumVitae curriculumVitae, int estado_carrera, EstadoCivil estadoCivil,
			Date fechaDeNacimiento, String instituto, int legajo_final, int legajo_inicio, String nacionalidad,
			long numeroDeDocumento, float promedioConAplazos, char sexo, char status, TipoDeDocumento tipoDeDocumento) {
		
		super(ciudad, codigo, dto, email, nombre, numero, pais, partido, piso, provincia, telefonoAlternativo,
				telefonoParticular, calle);
		this.carrera = carrera;
		this.apellido = apellido;
		this.cuil_DigitoVerificador = cuil_DigitoVerificador;
		this.cuil_dni = cuil_dni;
		this.cuil_DosDigitos = cuil_DosDigitos;
		this.curriculumVitae = curriculumVitae;
		this.estado_carrera = estado_carrera;
		this.estadoCivil = estadoCivil;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.instituto = instituto;
		this.legajo_final = legajo_final;
		this.legajo_inicio = legajo_inicio;
		this.nacionalidad = nacionalidad;
		this.numeroDeDocumento = numeroDeDocumento;
		this.promedioConAplazos = promedioConAplazos;
		this.sexo = sexo;
		this.status = status;
		this.tipoDeDocumento = tipoDeDocumento;
	}

	public Alumno(int pcod, String pNom, String pApe) {
		setCodigo(pcod);
		setNombre(pNom);
		setApellido(pApe);
	}

	/*----------------------------------GETTERS & SETTERS--------------------------------*/
	public Carrera getCarrera() {
		return carrera;
	}
	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getCuil_DigitoVerificador() {
		return cuil_DigitoVerificador;
	}
	public void setCuil_DigitoVerificador(int cuil_DigitoVerificador) {
		this.cuil_DigitoVerificador = cuil_DigitoVerificador;
	}
	public int getCuil_dni() {
		return cuil_dni;
	}
	public void setCuil_dni(int cuil_dni) {
		this.cuil_dni = cuil_dni;
	}
	public int getCuil_DosDigitos() {
		return cuil_DosDigitos;
	}
	public void setCuil_DosDigitos(int cuil_DosDigitos) {
		this.cuil_DosDigitos = cuil_DosDigitos;
	}
	public CurriculumVitae getCurriculumVitae() {
		return curriculumVitae;
	}
	public void setCurriculumVitae(CurriculumVitae curriculumVitae) {
		this.curriculumVitae = curriculumVitae;
	}
	public int getEstado_carrera() {
		return estado_carrera;
	}
	public void setEstado_carrera(int estado_carrera) {
		this.estado_carrera = estado_carrera;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	public String getInstituto() {
		return instituto;
	}
	public void setInstituto(String instituto) {
		this.instituto = instituto;
	}
	public int getLegajo_final() {
		return legajo_final;
	}
	public void setLegajo_final(int legajo_final) {
		this.legajo_final = legajo_final;
	}
	public int getLegajo_inicio() {
		return legajo_inicio;
	}
	public void setLegajo_inicio(int legajo_inicio) {
		this.legajo_inicio = legajo_inicio;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public long getNumeroDeDocumento() {
		return numeroDeDocumento;
	}
	public void setNumeroDeDocumento(long numeroDeDocumento) {
		this.numeroDeDocumento = numeroDeDocumento;
	}
	public float getPromedioConAplazos() {
		return promedioConAplazos;
	}
	public void setPromedioConAplazos(float promedioConAplazos) {
		this.promedioConAplazos = promedioConAplazos;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public TipoDeDocumento getTipoDeDocumento() {
		return tipoDeDocumento;
	}
	public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
		this.tipoDeDocumento = tipoDeDocumento;
	}

	public boolean isEmpty() {
		// TODO Nicolas hay que ver que algun atributo tenga datos caso contrario debe devolver verdadero 
		return false;
	}

	public boolean isModificable() {
		// TODO A designar, este metodo debe controlar que tenga por lo menos un atributo y entre ellos el codigo
		return true;
	}
}
/*----------------------------------EQUALS,HASHCODE,TOSTRING--------------------------------*/
	/*
	 * @Override
	public int hashCode() {
		return ;
	}

	@Override
	public boolean equals(Object obj) {
	//COMPARO SOLO EL ATRIBUTO CODIGO
		return super.equals(obj);
	}
	
	@Override	
	public String toString() {
		StringBuffer sb =new StringBuffer(super.toString());
		
		//sb.append("Ciudad=");
	
		
		//return sb.toString();
	}	
	*/



