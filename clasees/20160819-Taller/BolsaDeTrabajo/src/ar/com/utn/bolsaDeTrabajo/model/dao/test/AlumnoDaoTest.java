package ar.com.utn.bolsaDeTrabajo.model.dao.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Alumno;
import ar.com.utn.bolsaDeTrabajo.model.dao.AlumnoDao;
import ar.com.utn.bolsaDeTrabajo.model.exceptions.ModeloException;
import ar.com.utn.bolsaDeTrabajo.util.ConnectionManager;

public class AlumnoDaoTest {
	static Connection 	con		=	null;
	AlumnoDao 	aluDao	=	null;
	Alumno 		alu 	=	null;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConnection();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDaoTest.class.getResource( "CrearAlumnos.txt" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDaoTest.class.getResource( "EliminarAlumnos.txt" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        con.close();
		
	}

	@Before
	public void setUp() throws Exception {
		aluDao = new AlumnoDao(con);
	}

	@After
	public void tearDown() throws Exception {
		aluDao = null;
	}

	@Test
	public void testAgregar() throws SQLException, ModeloException {
		
		alu = new Alumno();
		alu.setNombre("Nombre_testAgregar_Test");
		alu.setApellido("Apellido_testAgregar_test");
		
		assertTrue(aluDao.agregar(alu));
		
	}

	@Test
	public void testEliminar() throws SQLException, ModeloException {
		//creo un objeto para buscarlo en la base
		alu = new Alumno();
		alu.setNombre("Gabriel4_test");
		alu.setApellido("Casas4_test");		
		List alumnos = aluDao.leer(alu);
		//obtengo un alumno y modifico su valor
		Alumno aluLeido= (Alumno)alumnos.get(0);
		
		assertTrue(aluDao.eliminar(aluLeido));
		
	}

	@Test
	public void testModificar() throws SQLException, ModeloException {
		//creo un objeto para buscarlo en la base
		alu = new Alumno();
		alu.setNombre("Gabriel2_test");
		alu.setApellido("Casas2_test");		
		List alumnos = aluDao.leer(alu);
		//obtengo un alumno y modifico su valor
		Alumno aluLeido= (Alumno)alumnos.get(0);
		aluLeido.setNombre("Gabriel2_modificado_test");
		assertTrue(aluDao.modificar(aluLeido));
		//verifico que el valor fue modificado
		assertTrue(aluDao.leer(aluLeido).size()>0);
		
	}

	@Test
	public void testLeer() throws SQLException, ModeloException {
		//creo un objeto para buscarlo en la base
		alu = new Alumno();
		alu.setNombre("Gabriel3_test");
		alu.setApellido("Casas3_test");		
		List alumnos = aluDao.leer(alu);
		
		Alumno alumno=(Alumno)alumnos.get(0);
		
		assertTrue(alumno.getCodigo()>0);
	}

}
