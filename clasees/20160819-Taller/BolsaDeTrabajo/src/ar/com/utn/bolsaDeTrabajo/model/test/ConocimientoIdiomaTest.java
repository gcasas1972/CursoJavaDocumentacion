package ar.com.utn.bolsaDeTrabajo.model.test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.ConocimientoIdioma;


public class ConocimientoIdiomaTest {
	
	public static ConocimientoIdioma conocimientoidioma;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		conocimientoidioma = new ConocimientoIdioma();
		conocimientoidioma.setCodigo(1);
		conocimientoidioma.setIdioma(null);
		conocimientoidioma.setNivelDeConocimiento(8);
	}
	
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(1, conocimientoidioma.getCodigo());
	}
		
	@Test
	public void setIdiomaTest() {
		assertEquals(null, conocimientoidioma.getIdioma());
	}
	
	@Test
	public void setNivelDeConocimientoTest() {
		assertEquals(8, conocimientoidioma.getNivelDeConocimiento());
	}

	@Test
	public void equalsTest_correcto() {
		ConocimientoIdioma conocimientoidioma1 = new ConocimientoIdioma(1, null, 8);
		Assert.assertTrue(conocimientoidioma.equals(conocimientoidioma1));
	}
	@Test
	public void equalsTest_falso() {
		ConocimientoIdioma conocimientoidioma2 = new ConocimientoIdioma(3, null, 5);
		Assert.assertFalse(conocimientoidioma.equals(conocimientoidioma2));
	}

}
