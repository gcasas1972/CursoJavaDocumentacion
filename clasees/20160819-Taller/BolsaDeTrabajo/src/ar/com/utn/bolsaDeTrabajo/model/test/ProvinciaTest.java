package ar.com.utn.bolsaDeTrabajo.model.test;


import static org.junit.Assert.assertEquals;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Provincia;

public class ProvinciaTest {

	public static Provincia provincia;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		provincia = new Provincia();
		provincia.setCodigo(4);
		provincia.setDescripcion("Buenos Aires");

	}

	@Test
	public void setCodigoTest() {
		assertEquals(4, provincia.getCodigo());
	}

	@Test
	public void setDescripcionTest() {
		assertEquals("Buenos Aires", provincia.getDescripcion());
	}

	@Test
	public void equals_correctoTest() {
		Provincia provincia2 = new Provincia(4, "Buenos Aires");
		Assert.assertTrue(provincia.equals(provincia2));
	}

	@Test
	public void equals_falsoTest() {
		Provincia provincia2 = new Provincia(2, "La Pampa");
		Assert.assertFalse(provincia.equals(provincia2));
	}

}
