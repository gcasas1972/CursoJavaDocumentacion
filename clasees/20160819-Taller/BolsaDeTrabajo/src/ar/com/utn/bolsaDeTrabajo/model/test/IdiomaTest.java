package ar.com.utn.bolsaDeTrabajo.model.test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.utn.bolsaDeTrabajo.model.Idioma;



public class IdiomaTest {
	
	public static Idioma idioma;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		idioma = new Idioma();
		idioma.setCodigo(89);
		idioma.setDescripcion("Franc�s");
	}
	@Test
	public void setCodigoTest() {
		Assert.assertEquals(89, idioma.getCodigo());
	}
		
	@Test
	public void setDescripcionTest() {
		assertEquals("Franc�s", idioma.getDescripcion());
	}

	@Test
	public void equalsTest_correcto() {
		Idioma idioma1 = new Idioma(89, "Franc�s");
		Assert.assertTrue(idioma.equals(idioma1));
	}
	@Test
	public void equalsTest_falso() {
		Idioma idioma2 = new Idioma(90, "Alem�n");
		Assert.assertFalse(idioma.equals(idioma2));
	}
}
