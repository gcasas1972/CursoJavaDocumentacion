package ar.com.utn.bolsaDeTrabajo.model;

import java.util.List;

/**
 * @author Gabriel
 * Esta clase representa los roles de los usuario por ejempl
 * operador, administrador
 */
/**
 * @author Gabriel
 *
 */
public class Rol {
	
	public Rol(){}
	public Rol (List accesos, int codigo ,String descripcion){
		super();
		this.codigo = codigo;    
		this.descripcion = descripcion;  
		this.accesos = accesos;
	}
	
	
	private List accesos;
	private int codigo;
	private String descripcion;
	public List getAccesos() {
		return accesos;
	}
	
	/**
	 * @param accesos
	 * este paramtr corresponde a una una lista de accesos
	 */
	public void setAccesos(List accesos) {
		this.accesos = accesos;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
	@Override
	public boolean equals(Object obj) {
		boolean bln = false;
		Rol ro = null;
		if (obj instanceof Rol){
			//down cast
			ro = (Rol)obj;
			bln = super.equals(obj) && 
			ro.getCodigo()==codigo;
			}
	return bln;
	}
	
	@Override
	public String toString() {
	StringBuffer sb =new StringBuffer(super.toString());
	//append suma
	sb.append("codigo=");
	sb.append(codigo);
	sb.append("descricion=");
	sb.append(descripcion);
	sb.append("accesos=");
	sb.append(accesos);	
	return sb.toString();
	
	
                        	}

	@Override
	public int hashCode() {
		return accesos.hashCode() + codigo +
			   descripcion.hashCode(); 	
		
		
		
	}
	
	
	
	
	
	
	
	
	
}
