package ar.com.utn.bolsaDeTrabajo.model;

public class OfertaLaboral {

	public OfertaLaboral(){}
	public OfertaLaboral(Cargo cargoACubrir, Contacto contactoEmpresa,
			Contacto contactoUTN, Empresa empresa, int eSTADO,
			JornadaLaboral jornadaLaboral, int referencia) {
		super();
		this.cargoACubrir = cargoACubrir;
		this.contactoEmpresa = contactoEmpresa;
		this.contactoUTN = contactoUTN;
		this.empresa = empresa;
		ESTADO = eSTADO;
		this.jornadaLaboral = jornadaLaboral;
		this.referencia = referencia;
		
	}
	private Cargo cargoACubrir;
	private Contacto contactoEmpresa;
	private Contacto contactoUTN;
	private Empresa empresa;
	private int ESTADO;
	private JornadaLaboral jornadaLaboral;
	private int referencia;
	
	public Cargo getCargoACubrir() {
		return cargoACubrir;
	}
	public void setCargoACubrir(Cargo cargoACubrir) {
		this.cargoACubrir = cargoACubrir;
	}
	public Contacto getContactoEmpresa() {
		return contactoEmpresa;
	}
	public void setContactoEmpresa(Contacto contactoEmpresa) {
		this.contactoEmpresa = contactoEmpresa;
	}
	public Contacto getContactoUTN() {
		return contactoUTN;
	}
	public void setContactoUTN(Contacto contactoUTN) {
		this.contactoUTN = contactoUTN;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public int getESTADO() {
		return ESTADO;
	}
	public void setESTADO(int eSTADO) {
		ESTADO = eSTADO;
	}
	public JornadaLaboral getJornadaLaboral() {
		return jornadaLaboral;
	}
	public void setJornadaLaboral(JornadaLaboral jornadaLaboral) {
		this.jornadaLaboral = jornadaLaboral;
	}
	public int getReferencia() {
		return referencia;
	}
	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}
	
	public boolean equals(Object obj){
		boolean bln=false;
		OfertaLaboral ofertaLaboral= null;		
		if(obj instanceof OfertaLaboral){
			ofertaLaboral=(OfertaLaboral) obj;
			bln =   ofertaLaboral.getCargoACubrir()==cargoACubrir &&
					ofertaLaboral.getContactoEmpresa()==contactoEmpresa &&
					ofertaLaboral.getContactoUTN()==contactoUTN &&
					ofertaLaboral.getEmpresa()==empresa &&
					ofertaLaboral.getESTADO()==ESTADO &&
					ofertaLaboral.getJornadaLaboral()==jornadaLaboral &&
					ofertaLaboral.getReferencia()==referencia ;
		
		}
		return bln;
	
	}
	
	@Override
	public int hashCode() {		
		return  cargoACubrir.hashCode() + 
				contactoEmpresa.hashCode() +
				contactoUTN.hashCode() +
				empresa.hashCode() +
				jornadaLaboral.hashCode();
	}		
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("cargoACubrir=");
		sb.append(cargoACubrir);
		sb.append(",contactoEmpresa=");
		sb.append(contactoEmpresa);
		sb.append(",contactoUTN=");
		sb.append(contactoUTN);		
		sb.append(",empresa=");
		sb.append(empresa);	
		sb.append(",ESTADO=");
		sb.append(ESTADO);	
		sb.append(",jornadaLaboral=");
		sb.append(jornadaLaboral);	
		sb.append(",referencia=");
		sb.append(referencia);	
		return sb.toString();
	}
}
