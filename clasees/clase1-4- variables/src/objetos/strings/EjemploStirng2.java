package objetos.strings;

public class EjemploStirng2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = new String("Hola");
		//String es inmutable
		//debo usar otra variable y guardar el resultado
		
		String sMayuscula= new String(s.toUpperCase());
		
		System.out.println("s="+s);
		System.out.println("sMayuscula="+ sMayuscula);

	}

}
