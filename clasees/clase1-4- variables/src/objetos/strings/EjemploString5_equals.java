package objetos.strings;

public class EjemploString5_equals {
	public static void main(String[] args) {
	//en este caso tengo dos objetos
	String s = new String("Hola");
	String h = new String("Hola");
	if(s==h)
		System.out.println("tiene la misma direccion");	
	else
		System.out.println("tienen direcciones diferentes");

	//voy a usar el mismo obteto
	String w = s;
	if(s==w)
		System.out.println("tiene la misma direccion");	
	else
		System.out.println("tienen direcciones diferentes");

	// para comparar el contenido
	if(s.equals(h))
		System.out.println("tienen el mismo contenido");
	else
		System.out.println("tienen contenido diferente");
}
}
