package objetos.strings;

public class EjemploString6_funciones {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = new String("Hola 123 Gabrielito 456");
		//					   01234567890123456789012	
		for(int i=0;i<s.length();i++){
			System.out.print("s("+ i +")="+ s.charAt(i));
			System.out.println(" num="+ esNumero(s.charAt(i)));
		}
	}
	
	private static boolean esNumero(char parNum){
	//me deveuelve verdadero si el paramtro es un numero
	//falso si es otra cosa
		
		//me devuelve un valor boolean
		//le envio un parametro parNUm
		
		boolean bResult= Character.isDigit(parNum);
		
		return bResult;
		
	}

}
