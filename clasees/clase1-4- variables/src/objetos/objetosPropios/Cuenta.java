package objetos.objetosPropios;

public abstract class Cuenta {
	private int numero;
	private float saldo;

	public Cuenta() {}	
	public Cuenta(int pNum, float pValor) {
		numero=pNum;
		saldo = pValor;
	}
	
	
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setNumero(int pNum)	{
		//aca puedo validar lo que se me cante
		// si existe erro sysout
		numero = pNum;		
	}
	public int getNumero()			{	return numero;	}
	
	public void acreditar(float pValor){
		saldo= saldo + pValor;		
	}
	
	public abstract void debitar(float pValor);
	
	
	public boolean equals(Object obj){
		boolean bln=false;
		if(obj instanceof Cuenta){
			//hago el downcast para poder ver los metodos 
			Cuenta cue=(Cuenta)obj; // castear a tipo cuenta	
			//mi numero igual al numero que recibo por parametro
			bln = numero==cue.getNumero();
		}
		
		return bln;
	}
	
	public int hashCode(){
		return numero;
	}
	public String toString(){
		//String st = "numero="+numero+ "saldo="+ saldo;
		StringBuffer sb = new StringBuffer("numero=");
		//suma texto
		sb.append(numero);
		sb.append(";saldo=");
		sb.append(saldo);
		return sb.toString();
	}

}
