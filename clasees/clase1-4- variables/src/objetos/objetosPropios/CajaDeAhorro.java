package objetos.objetosPropios;

public class CajaDeAhorro extends Cuenta {
	private float interes;
	
	public CajaDeAhorro() {
		super();
	}

	public CajaDeAhorro(int pNum, float pValor) {
		super(pNum, pValor);
	}

	public CajaDeAhorro(int pNum, float pSaldo, float pInteres) {
		this(pNum, pSaldo);
		interes = pInteres;		
	}

	public float getInteres() {
		return interes;
	}

	public void setInteres(float interes) {
		this.interes = interes;
	}
	
	public boolean equals(Object obj){
		boolean bln=false;
		if(obj instanceof CajaDeAhorro){
			//downcast			
			CajaDeAhorro ca =(CajaDeAhorro) obj;			
			bln = super.equals(obj) && interes==ca.getInteres();
		}
		return bln;
	}
	
	@Override
	public void debitar(float pValor) {
		if (pValor < getSaldo())
			setSaldo(getSaldo()-pValor);
		
	}

	public int hashCode(){
		return super.hashCode()+ (int)interes;
	}
	public String toString(){
		StringBuffer sb = new StringBuffer(super.toString());
		sb.append(";interes=");
		sb.append(interes);		
		return sb.toString();
	}

}
