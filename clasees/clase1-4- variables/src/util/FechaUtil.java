package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FechaUtil {

	/**
	 * Esta funci�n tiene la finalidad de determinar
	 * si la fecha es o no un dia del fin de semana
	 * @param pFecha es la fecha a analizar
	 * @return verdadero si es fin de semana y false si es dia 
	 * de semana
	 */
	public static boolean isFinDeSemana(Date pFecha){
		//creo un objeto de tipo Calendar
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		return cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY ||
			   cal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY;
			
	}
	public static Date asDate(String pFecha){
		//dd/MM/YYYY
		//0123456789
		Calendar cal = Calendar.getInstance();
		//es el primero inclusive y el segundo exclusive
		int dia= Integer.parseInt(pFecha.substring(0, 2));
		int mes= Integer.parseInt(pFecha.substring(3, 5));
		int anio= Integer.parseInt(pFecha.substring(6, 10));
		cal.set(anio, mes-1, dia);
		
		return cal.getTime();
		
	}
	public static int asInt(String pFecha){
		String total= 	pFecha.substring(6, 10)+
						pFecha.substring(3, 5) +
						pFecha.substring(0, 2) ;
		
		return Integer.parseInt(total);
		
	}
}

