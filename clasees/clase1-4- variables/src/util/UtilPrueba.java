package util;

public class UtilPrueba {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("hola,false=" +
								StringUtil.isEspacioDoble("hola"));
		System.out.println("ho  la,true=" +
								StringUtil.isEspacioDoble("ho  la"));
		System.out.println("Gabriel,false=" + 
								StringUtil.isNumero("Gabriel"));
		System.out.println("Gabriel123,true=" + 
								StringUtil.isNumero("Gabriel123"));
	}

}
