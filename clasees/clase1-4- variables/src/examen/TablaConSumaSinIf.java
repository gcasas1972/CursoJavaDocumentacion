package examen;

public class TablaConSumaSinIf {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int cont=0;
		int num=3;
		int res=0;
		for (int i = 1; i < 11; i++) {
			res=num*i;
			System.out.println(num+"x"+i+"="+res);
			//si es par el resto da 0 y si es impar el resto da 1
			//si resto 1 da        -1 y                         0
			//si mult por -1 da     1 y							0 
			cont=cont+res *(res%2-1)*(-1);
		}
		
		System.out.println("la suma de los valores pares es: "+cont);

	}

}
