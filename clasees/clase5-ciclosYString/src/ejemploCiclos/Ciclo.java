package ejemploCiclos;

public class Ciclo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//voy a recorrer 10 veces
		
		int i=0;
		System.out.println("ciclo while");
		while(i<10){
			System.out.println("i="+i);
			
			switch (i) {
			case 1:
				System.out.println(i + " oro");
				break;
			case 2:
				System.out.println(i + " plata");
				break;
			case 3:
				System.out.println(i + " bronce");
				break;			

			default:
				System.out.println(i + "siga participando");
				break;
			}
			i++;
		}
		i=0;
		
		System.out.println("ahora con ciclo for");
		for(i=0;i<10;i++){
			System.out.println("i="+i);
			if(i==5)
				break;
		}
		i=0;
		System.out.println("do while...");
		char letra ='a';
		int iletra = letra;
		do{
			char otraLetra=(char)(iletra++);
			System.out.println(otraLetra);
			switch (otraLetra) {
			case 'a':
				System.out.println(otraLetra+ " sos groso");
				break;
			case 'b':
				System.out.println(otraLetra+ " sos menos groso");
				break;
			case 'c':
				System.out.println(otraLetra+ " sos menos menos");
				break;
			default:
				System.out.println(otraLetra+ " sos del monton");
				break;
			}
			i++;
		}while(i<10);
		
		}
			

	}


