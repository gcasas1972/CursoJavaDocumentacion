package string;

public class EjemploString2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str = new String("Gabrielito");
							//	 0123456789
		//objetivo: escribir el Stirng de arriba para abajo
		//length me la longitud del String
		//char at, me devuelve un caracter
		//indexof, me devuelve la posicion
		//substring el primero es inclusive
		// es segundo es exclusive
		
		System.out.println(str.substring(2,5));
		for(int i=0;i<str.length();i++)
			System.out.println(str.charAt(i));
		
		//cual es la posicion de la i
		System.out.println("la i esta en ="+ str.indexOf('i'));
	}

}
