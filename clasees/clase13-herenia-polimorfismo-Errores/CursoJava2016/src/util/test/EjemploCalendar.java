package util.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EjemploCalendar {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//creo un objeto con la fecha de hoy
		Calendar cal = Calendar.getInstance();
		//le asigno la fecha de mi cumple.
		cal.set(1972, Calendar.NOVEMBER, 17);
		//cal.set(1972, Calendar.NOVEMBER, 17,13,15);
		SimpleDateFormat sdf = 
			new SimpleDateFormat("yyyy-MM-dd EEEEEE HH:mm");
		
		
		System.out.println(	"la fecha de mi" +
							" nacimiento es "+
							sdf.format(cal.getTime()));
		
		System.out.println("El a�o es " + 
				cal.get(Calendar.YEAR) );
		//el mes siempre me da uno de menos
		System.out.println(
				"el mes es " +
				(cal.get(Calendar.MONTH)+1)
				);
		
		System.out.println("el dia es " +
				cal.get(Calendar.DATE));
		
		

	}

}
