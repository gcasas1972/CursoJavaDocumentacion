package stringPractica2;

public class StringEjemplo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str = new String("aeiou ttt");
		int cantVocales=0;
		int cantConso=0;
		int cantEspacios=0;		
		int i=0;
		//for(int i=0;i<str.length();i++){
		while (i<str.length()){
			char letra= str.toLowerCase().charAt(i);
			switch (letra) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				cantVocales++;				
				break;				
			case ' ':
				cantEspacios++;
				break;
			default:
				cantConso++;
				break;
			}
		   i++;			
		}
		System.out.println("vocales="+ cantVocales);
		System.out.println("consonantes="+ cantConso);
		System.out.println("Espacios="+ cantEspacios);

	}

}
