package string;

public class EjemploString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = new String("Gabrielito");
		String s2= new String("Gabrielito");
		//String s2=s;
		//son distintos porque compara referencias
		if(s==s2)
			System.out.println("las referencias son iguales ");					
		else
			System.out.println("las referencias son distintas");
		
		//ahora
		if(s.equals(s2))
			System.out.println("los cont, son iguales");
		else 
			System.out.println("los cont. son distintos");

	}

}
