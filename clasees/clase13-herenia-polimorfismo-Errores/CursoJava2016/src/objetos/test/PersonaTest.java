package objetos.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import objetos.Alumno;
import objetos.Persona;
import objetos.exceptions.LegajoException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PersonaTest {
	//lo defino para que pueda ser utilizado
	Persona per=null;
	Alumno 	alu = null		, 
			aluPerez1=null	, 
			aluPerez2=null  , 
			aluCasas=null	;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	@Before
	public void setUp() throws Exception {
		per= new Persona();
		per.setNombre("Gabrielito");
		//creo el modelo de testeo
		aluPerez1 = new Alumno(	"Juan"	, 
								"Perez"	, 
								null	, 
								10		);
		aluPerez2 = new Alumno(	"Juan"	, 
								"Perez"	, 
								null	, 
								10		);
		aluCasas = new Alumno(	"Gabriel", 
								"Casas"	, 
								null	, 
								10		);
		
		
		
		
		
	}
	@Test
	public void testGetNombre() {
	 assertEquals("Gabrielito", per.getNombre());
	}

	@Test
	public void testSetNombre_null() {
		per.setNombre("ga  briel");
		assertNull(per.getNombre());	
	}
	@Test
	public void testSetNombre_ConValor() {
		per.setNombre("Gabriel Casas");
		assertEquals("Gabriel Casas", per.getNombre());
		
	}
	@Test
	public void Alumno_vacio(){
		alu = new Alumno();
		assertEquals("Gabrielito", alu.getNombre());
		
	}
	@Test
	public void Alumno_StringString() throws LegajoException{
	alu = new Alumno("Gabriel", "Casas");
	assertEquals("Gabriel", alu.getNombre());
	assertEquals("Casas", alu.getApellido());
	assertNull(alu.getFechaNacimiento());
	assertEquals(10,alu.getLegajo());
	}
	@Test
	public void Alumno_equals_TRUE(){
		assertTrue(aluPerez1.equals(aluPerez2));
	}
	@Test
	public void Alumno_equals_FALSE(){
		assertFalse(aluPerez1.equals(aluCasas));
	}
	@Test
	public void Alumno_Arrylist() throws LegajoException{
		ArrayList<Alumno> alumnos= 
			    new ArrayList<Alumno>();
		
		alumnos.add(new Alumno("Sebastian"	, 
								"Donato"	, 
								null		, 
								10))		;
		
		alumnos.add(new Alumno("German"		, 
								"Pallota"	, 
								null		, 
								11))		;
		
		alumnos.add(new Alumno("Daniela"	, 
								"Gonzalez"	, 
								null		, 
								12))		;
		alumnos.add(new Alumno("Juan"		, 
								"Perez"		,
								null		, 
								10));
		alumnos.add(new Alumno("Juan"		, 
								"Perez"		,
								null		, 
								10))		;
		assertEquals(5, alumnos.size());
		
	}
	@Test
	public void Alumno_Arrylist_conteins() throws LegajoException{
		ArrayList<Alumno> alumnos= 
		    new ArrayList<Alumno>();
	
	alumnos.add(new Alumno("Sebastian"	, 
							"Donato"	, 
							null		, 
							10))		;
	
	alumnos.add(new Alumno("German"		, 
							"Pallota"	, 
							null		, 
							11))		;
	
	alumnos.add(new Alumno("Daniela"	, 
							"Gonzalez"	, 
							null		, 
							12))		;
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10));
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10))		;
	assertTrue(alumnos.contains(aluPerez1));
	
	}
	@Test
	public void Alumno_Hashset_conteins() throws LegajoException{
		HashSet<Alumno> alumnos= 
		    new HashSet<Alumno>();
	
	alumnos.add(new Alumno("Sebastian"	, 
							"Donato"	, 
							null		, 
							10))		;
	
	alumnos.add(new Alumno("German"		, 
							"Pallota"	, 
							null		, 
							11))		;
	
	alumnos.add(new Alumno("Daniela"	, 
							"Gonzalez"	, 
							null		, 
							12))		;
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10));
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10))		;
	assertTrue(alumnos.contains(aluPerez1));
	
	}
	@Test
	public void Alumno_Hashset() throws LegajoException{
		HashSet<Alumno> alumnos= 
		    new HashSet<Alumno>();
	
	alumnos.add(new Alumno("Sebastian"	, 
							"Donato"	, 
							null		, 
							10))		;
	
	alumnos.add(new Alumno("German"		, 
							"Pallota"	, 
							null		, 
							11))		;
	
	alumnos.add(new Alumno("Daniela"	, 
							"Gonzalez"	, 
							null		, 
							12))		;
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10));
	alumnos.add(new Alumno("Juan"		, 
							"Perez"		,
							null		, 
							10))		;
	assertEquals(4, alumnos.size());
	
	}
		
	@Test
	public void Alumno_LegajoTest_Exitoso(){
		Alumno aluLeg = new Alumno();
		
		try {
			aluLeg.setLegajo(10);
			assertEquals(10, aluLeg.getLegajo());
		} catch (LegajoException e) {
			assertTrue(false);
		} catch (Exception e) {
			assertTrue(false);
		}
		
	}
	@Test
	public void Alumno_LegajoTest_Fallido(){
		Alumno aluLeg = new Alumno();
		try {
			aluLeg.setLegajo(-10);
			assertEquals(-10, aluLeg.getLegajo());
		} catch (LegajoException e) {
			assertTrue(true);
			assertEquals("el legajo no puede ser negativo ni cero", 
					e.getMessage());
		} catch (Exception e) {
			assertTrue(false);
		}
		
		
	}
	
	
	

}
