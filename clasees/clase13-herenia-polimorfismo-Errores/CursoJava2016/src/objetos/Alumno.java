package objetos;

import java.util.Date;

import objetos.exceptions.LegajoException;

public class Alumno extends Persona {
	private int legajo;
	public Alumno(){
		super("Gabrielito");
	}
	public Alumno(String nom, String ape) 
				throws LegajoException{
		this(nom,ape,null, 10);
	}
	public Alumno(String nom, String ape,
			Date fecha, int leg) throws LegajoException{
		
		setNombre(nom);
		setApellido(ape	);
		setFechaNacimiento(fecha);
		setLegajo(leg);
	}
	
	
	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) 
						throws LegajoException {
		
		if(legajo<=0)
			throw new LegajoException(
					"el legajo no puede ser negativo ni cero" 
					);
		
		this.legajo = legajo;
	}
	@Override
	public boolean equals(Object obj) {
		boolean bln=false;
		Alumno alu=null;
			
		if (obj instanceof Alumno){
			//down cast
			alu=(Alumno)obj;
			bln=super.equals(obj) &&
				legajo == alu.getLegajo();
		}	
		return bln;
	}
	@Override
	public int hashCode() {
		return super.hashCode()+legajo;
	}
	@Override
	public String toString() {
		StringBuffer sb = 
			new StringBuffer(super.toString());
		sb.append(", legajo=");
		sb.append(legajo);
		return sb.toString();
	}
	
}
