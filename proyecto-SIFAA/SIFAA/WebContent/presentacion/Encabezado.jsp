<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Sistema SIFAA</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <!-- animated css-->

    <link rel="stylesheet" href="/css/animate.min.css">

    <!-- Magnific Popup core CSS file -->

    <link rel="stylesheet" href="magnific-popup/magnific-popup.css">

    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Macondo" rel="stylesheet">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	
    <!-- main css-->

    <link rel="stylesheet" href="css/main.css">
    
    <div class="container">
  		<img src="/SIFAA/fotos/logo.png" class="img-rounded" alt="Cinque Terre" width="404" height="436"> 
	</div>
</head>
