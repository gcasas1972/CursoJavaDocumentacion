package ar.com.inac.sifaa.util;

import java.net.URL;

import org.hibernate.*;
import org.hibernate.cfg.*;
import org.hibernate.util.ConfigHelper;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    

    static {
        try {
			URL url = ConfigHelper.findAsResource("resources/hibernate.cfg.xml");
						
            sessionFactory 			= new Configuration().configure(url).buildSessionFactory();
            
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
                                                  
    public static SessionFactory getSessionFactory()throws HibernateException {
        return sessionFactory;
    }
    
}