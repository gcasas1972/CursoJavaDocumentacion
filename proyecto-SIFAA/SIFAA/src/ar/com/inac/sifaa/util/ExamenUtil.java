package ar.com.inac.sifaa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExamenUtil {
	public void cargarEvaluacion(String path){
		   File file = new File(path);

		    try {
		      // Create a buffered reader to read each line from a file.
		      BufferedReader in = new BufferedReader(new FileReader(file));
		      String s;

		      // Read each line from the file and echo it to the screen.
		      s = in.readLine();
		      while (s != null) {
			System.out.println(s);
		        s = in.readLine();
		      }
		      // Close the buffered reader, which also closes the file reader.
		      in.close();

		    } catch (FileNotFoundException e1) {
		    // If this file does not exist
		      System.err.println("File not found: " + file);

		    } catch (IOException e2) {
		    // Catch any other IO exceptions.
		      e2.printStackTrace();
		    }
		
		
	}
	public void generarEvaluacion(){
		
	}

}
