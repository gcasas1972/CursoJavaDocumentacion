package ar.com.inac.sifaa.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class DateUtil {
   
   public static final String LARGE_LOG_FORMAT = "yyyyMMddHHmmssSSS";
   public static final String LARGE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
   public static final String LARGE_FORMAT     = "yyyy-MM-dd HH:mm:ss.SSS";
   public static final String SHORT_FORMAT     = "yyyy-MM-dd HH:mm:ss";
   public static final String FECHA_FORMAT     = "yyyy-MM-dd";
   private static final Locale LOCALE = new Locale( "es", "AR" );
   private static TimeZone TIME_ZONE  = TimeZone.getTimeZone( "AGT" );

   public static Date getStringAsDate( String stringDate ) throws ParseException {
      if ( stringDate.length() == LARGE_FORMAT.length() ) {
         return parse( LARGE_FORMAT, stringDate );
      } else if (  stringDate.length() == SHORT_FORMAT.length() ) {
         return parse( SHORT_FORMAT, stringDate );
      } else { //if (  stringDate.length() == FECHA_FORMAT.length() ) {
         return parse( FECHA_FORMAT, stringDate );
      }
   }


   public static int asInt( String inPattern, String outPattern, String date ) throws ParseException {
      return asInt( format( outPattern, asDate( inPattern , date ) ) );
   }
   public static int asInt( String pattern, Date date ) {    return asInt( format( pattern, date ) ); }
   public static int asInt( String date ) {
      return Integer.parseInt( date );
   }

   // tolera hablar de formatos propietarios de 'periodos': 
   //    mensuales del tipo "200705" o 
   //    anuales de tipo "200700"
   public static Date asDate( String pattern, String date ) throws ParseException {
      try {
         String legalPattern = proDateFormat2javaDateFormat( pattern );
         // sin mes, ni dia
         if ( !legalPattern.contains( "MM" ) && !legalPattern.contains( "00" ) ) {
            return parse( legalPattern + "MMdd", date + "1231" );
         }
         
         // sin dia
         if ( !legalPattern.contains( "dd" ) ) {
            String legalDate;
            // como MM o como 00
            if ( legalPattern.startsWith( "00" ) && date.startsWith( "00" ) ) {
               legalDate = "12" + date.substring( 2 );
            } else if ( legalPattern.endsWith( "00" ) && date.endsWith( "00" ) ) {
               legalDate = date.substring( 0, date.length() - 2 ) + "12";
            } else {
               legalDate = date;
            }
            legalPattern = legalPattern.replace( "00", "MM" ) + "dd";
            
            return parse( legalPattern, legalDate + (getCalendar( parse( legalPattern, legalDate + "01" ) ).getActualMaximum( Calendar.DAY_OF_MONTH ) ) );
         }
         
         return  parse( legalPattern , date );
      } catch (ParseException e) {
         ParseException pe = new ParseException( e.getMessage() + " Contenido original [" + date + "] formato original [" + pattern + "].", e.getErrorOffset() );
         pe.setStackTrace( e.getStackTrace() );
         throw pe;
      }
         
   }

   static public String proDateFormat2javaDateFormat( String proFormat ) {
      // permite adaptar formatos propietarios como AAMMDD a los tipicos de Java
      return proFormat.toLowerCase()
                  .replaceAll( "a", "y" )
                  .replaceAll( "mm", "MM" )
                  .replaceAll( "hMM", "hmm" )
                  .replaceAll( "h:MM", "h:mm" )
                  .replaceAll( "h", "H" )
                  .replaceAll( "sss", "SSS" );
   }

   public static String asString( String inPattern, String outPattern, String date ) throws ParseException {
      return format( outPattern, asDate( inPattern , date ) );
   }

   public static String getSQLDateString( Date date ) {           return format( FECHA_FORMAT, date );         }
   public static String getDateTimeString( Date date ) {          return format( "dd-MM-yyyy HH:mm", date );   } 
   public static String getDateTimeSecondString( Date date ) {    return format( "d-MM-yyyy H:mm:ss", date );  }
   public static String getStringDate( Date date ){               return format( "yyyyMMddHHmm", date );       }
   public static String asString( Date date ) {                   return format( "yyyyMMdd", date );           }
   public static String asString( String pattern, Date date ) {   return format( pattern, date );              }
   public static String format( String pattern, Date date ) {
      return new SimpleDateFormat( proDateFormat2javaDateFormat( pattern ) ).format( date );
   }

   public static Date parse( String pattern, String date ) throws ParseException {
      try {
         return getStrictDateFormat( pattern ).parse( date );
      } catch (ParseException e) {
         throw new ParseException( "El contenido [" + date + "] no es una fecha valida segun el formato [" + pattern + "].", 
                                   e.getErrorOffset() );
      }
   }


   public static SimpleDateFormat getStrictDateFormat( String pattern ) {
      SimpleDateFormat sdf = new SimpleDateFormat( pattern, LOCALE  );
      sdf.setLenient( false );
      return sdf;
   }

   public static Calendar getCalendar() {
      return Calendar.getInstance( TIME_ZONE, LOCALE );
   }

   public static Calendar getCalendar( Date date ) {
      Calendar cal = getCalendar();
      cal.setTime( date );
      return cal;
   }    

   public static boolean after( Date date1, Date date2 ) {             return trunc( date1 ).after(  trunc( date2 ) );       }
   public static boolean before( Date date1, Date date2 ) {            return trunc( date1 ).before( trunc( date2 ) );       }
   public static boolean isDatePlusMonthsBeforeSecondDate( Date date1, int months, Date date2 ) {
      return trunc( add( date1, Calendar.MONTH, months ) ).before( trunc( date2) );
   }

   // inclusivo, date no puede ser null
   public static boolean isBetween( Date date, Date from, Date to ) {
      Date fecha = trunc( date );
      
      return ( from == null || !trunc( from ).after( fecha ) ) && 
             ( to   == null || !trunc( to  ).before( fecha ) );
   }

   public static Long differenceInDays( Date date1, Date date2 ) {
      return new Long( ( DateUtil.trunc( date1 ).getTime() - 
                         DateUtil.trunc( date2 ).getTime() ) / ( 1000 * 60 * 60 * 24 ) );
   }

   public static Date addMonths( Date date, int months ) {  return add( date, Calendar.MONTH, months );    }
   public static Date addDays( Date date, int dias ) {      return add( date, Calendar.DATE, dias );       }
   public static Date todayAdd( int field, int cant ) {     return add( new Date(), field, cant );         }
   public static Date add( Date date, int field, int cant ) {
      Calendar cal = getCalendar( date );
      cal.add( field, cant );
      return cal.getTime();
   }

   public static Date trunc( Date date ) {        
      // seguro hay forma mas eficiente
      try {
         return parse( FECHA_FORMAT, format( FECHA_FORMAT, date ) );
      } catch (Exception e) {
         // No es posible que de una excepcion
         return null;
      }
   }
   
   public static boolean isWeekend( Date fecha ) {
      Calendar cal = getCalendar( fecha );
      return isSaturday( cal ) || isSunday( cal );
   }

   public static boolean isSaturday( Calendar calFecha ) {
      return calFecha.get( Calendar.DAY_OF_WEEK ) == Calendar.SATURDAY;
   }

   public static boolean isSunday( Calendar calFecha ) {
      return calFecha.get( Calendar.DAY_OF_WEEK ) == Calendar.SUNDAY;
   }

}
