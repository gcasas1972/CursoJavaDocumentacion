package ar.com.inac.sifaa.util.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ar.com.inac.sifaa.util.FileUtil;

public class FileUtilTest {
	String nomArchPrueba=null;

	@Before
	public void setUp() throws Exception {
		nomArchPrueba = new String ("archivo.bmp");
	}

	@Test
	public void testGetExtension() {
		assertEquals("bmp", FileUtil.getExtension(nomArchPrueba));
	
	}

}
