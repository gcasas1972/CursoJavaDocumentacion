package ar.com.inac.sifaa.modelo.test;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import ar.com.inac.sifaa.modelo.Partido;
import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.util.DateUtil;

public class PersonaTest {
	Persona perNom ;
	Persona perCompleto;
	@Before
	public void setUp() throws Exception {
		perNom = new Persona("Gabriel");
		perCompleto = new Persona(10, "Gabriel",
								"Casas", "Solari 3866", 
								new Partido("Moron"), 
								"4697-1533", "1560057247", 
								"22965726", "20-22965726-8", 
								"1234567890", DateUtil.asDate("ddMMyyyy", "17111972"),
								"gcasas1972@gmail.com", "101020", 
								"aula", "c:/fotos");
		
		
	}

	@Test
	public void testEqualsObject_nombre() {
		Persona per = new Persona();
		per.setNombre("Gabriel");
		assertTrue(per.equals(perNom));
		
	}

	@Test
	public void testEqualsObject_TodosLosCampos() throws ParseException {
		Persona per = new Persona(10, "Gabriel",
				"Casas", "Solari 3866", 
				new Partido("Moron"), 
				"4697-1533", "1560057247", 
				"22965726", "20-22965726-8", 
				"1234567890", DateUtil.asDate("ddMMyyyy", "17111972"),
				"gcasas1972@gmail.com", "101020", 
				"aula", "c:/fotos");
			assertTrue(per.equals(perCompleto));
		
	}

	
}
