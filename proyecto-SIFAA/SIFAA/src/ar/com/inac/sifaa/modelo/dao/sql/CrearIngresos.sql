DROP TABLE IF EXISTS `sifaa`.`ingresos`;
CREATE TABLE  `sifaa`.`ingresos` (
  `ING_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PER_ID` int(10) unsigned NOT NULL,
  `ING_FECHAHORAINGRESO` datetime NOT NULL,
  `ING_FECHAHORASAL` datetime NOT NULL,
  `ING_MOTIVO` varchar(45) NOT NULL,
  PRIMARY KEY (`ING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;