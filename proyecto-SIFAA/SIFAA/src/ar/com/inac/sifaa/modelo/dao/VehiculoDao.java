package ar.com.inac.sifaa.modelo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

//TODO Se debe modificar para utilizar objeto vehÝculos.
//import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.Vehiculo;

public class VehiculoDao implements DAO{
	
	public Object agregarModificar (Object obj, Session session) throws Exception {
		if (session != null){
			try{
				obj=session.merge(obj)			;
				System.out.println("obj="+obj)	;
				return obj						;
			} catch (Exception e){
				e.printStackTrace();
				throw e;
			}
		} else return false;
	}

	public Object eliminar(Object obj, Session session) throws Exception {
		if (session!=null){			
			try {
				session.delete(obj);
				return obj;
				//getSession().flush();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}return false;
	}
	
	public List leer(Object obj, Session session) throws Exception {
		List l = null;
		
		if (session!=null){
			if( !vacio((Vehiculo)obj)){
				Vehiculo vehiculo	= 	null;
				
					vehiculo = (Vehiculo)obj;
					Criteria criteria=session.createCriteria(Vehiculo.class);
					if (vehiculo.getCodigo()>0)
						criteria.add(Restrictions.eq("codigo", vehiculo.getCodigo()));
					if (vehiculo.getMarca()!=null && !vehiculo.getMarca().isEmpty())
						criteria.add(Restrictions.eq("marca", vehiculo.getMarca()));					

					if (vehiculo.getModelo()!= null && !vehiculo.getModelo().isEmpty() )
						criteria.add(Restrictions.ilike("modelo", vehiculo.getModelo()+ "%"));
					
					if (vehiculo.getColor()!= null && !vehiculo.getColor().isEmpty() )
						criteria.add(Restrictions.ilike("color", vehiculo.getColor()+ "%"));
					
					if (vehiculo.getColor()!= null && !vehiculo.getDominio().isEmpty() )
						criteria.add(Restrictions.ilike("dominio", vehiculo.getDominio()+ "%"));
					
					l=criteria.list();
			}			
			else{	l=session.createQuery("from Vehiculo").list();	}
		}
		return l;	
	}
	
	private boolean vacio(Vehiculo vehiculo){		
		return 	vehiculo	==	null 			||
				vehiculo.getCodigo()	 		== 0 	;	 
	}
	
}
