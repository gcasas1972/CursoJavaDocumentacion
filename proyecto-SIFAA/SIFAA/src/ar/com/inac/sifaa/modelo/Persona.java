package ar.com.inac.sifaa.modelo;

import java.util.Date;

public class Persona {
	private int 		codigo		;
	private String 		nombre		;
	private String 		apellido	;
	private String 		direccion	;
	
	private Partido 	partido		;	
	private String 		telefonoParticular	;
	private String 		telefonoCelular		;	
	private String		dni			;
	
	private String		cuil		;
	private String 		codigoDeBarras		;
	private Date		fechaDeNacimiento;
	private String		email		;
	
	private String 		iosfa		;
	private String 		lugarDeTrabajo;
	private String 		pathFoto	;
	
	//constructores
	public Persona() {
		codigo = -1;
	}

	public Persona(String pNom) {
		codigo = -1;
		nombre = pNom;
	}
	public Persona(String pNom, String pApe) {
		codigo 		= -1;
		nombre 		= pNom;
		apellido 	= pApe;
	}
	public Persona(String pNom, String pApe, String pDir, String pTel) {
		codigo 		= -1;
		nombre 		= pNom;
		apellido 	= pApe;
		direccion 	= pDir;
		telefonoParticular 	= pTel;
	}
	
	public Persona(int codigo, String nombre, String apellido,
			String direccion, Partido partido, String telefonoParticular,
			String telefonoCelular, String dni, String cuil,
			String codigoDeBarras, Date fechaDeNacimiento, String email,
			String iosfa, String lugarDeTrabajo, String pathFoto) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.partido = partido;
		this.telefonoParticular = telefonoParticular;
		this.telefonoCelular = telefonoCelular;
		this.dni = dni;
		this.cuil = cuil;
		this.codigoDeBarras = codigoDeBarras;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.email = email;
		this.iosfa = iosfa;
		this.lugarDeTrabajo = lugarDeTrabajo;
		this.pathFoto = pathFoto;
	}
	public Persona(int pCod) {
		codigo = pCod;
	}

	public int getCodigo() {							return codigo;						}
	public void setCodigo(int codigo) {					this.codigo = codigo;				}
	
	public String getNombre() {							return nombre;						}
	public void setNombre(String nombre) {				this.nombre = nombre;				}
	
	public String getApellido() {						return apellido;					}
	public void setApellido(String apellido) {			this.apellido = apellido;			}
	
	public String getDireccion() {						return direccion;					}
	public void setDireccion(String direccion) {		this.direccion = direccion;			}
	
	public Partido getPartido() {						return partido;						}
	public void setPartido(Partido localidad) {			this.partido = localidad;			}

	public String getTelefonoParticular() {				return telefonoParticular;			}
	public void setTelefonoParticular(String telefono) {this.telefonoParticular = telefono;	}
	
	public String getDni() {							return dni;							}
	public void setDni(String dni) {					this.dni = dni;						}
	
	public String getCuil() {									return cuil;				}
	public void setCuil(String cuil) {							this.cuil = cuil;			}
	
	public String getCodigoDeBarras() {						return codigoDeBarras;					}
	public void setCodigoDeBarras(String codigoDeBarras) {	this.codigoDeBarras = codigoDeBarras;	}
	
	public Date getFechaDeNacimiento() {						return fechaDeNacimiento;					}
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {	this.fechaDeNacimiento = fechaDeNacimiento;	}
	
	public String getEmail() {							return email;						}
	public void setEmail(String email) {				this.email = email;					}
	
	public String getTelefonoCelular() {						return telefonoCelular;					}
	public void setTelefonoCelular(String telefonoCelular) {	this.telefonoCelular = telefonoCelular;	}
	
	public String getIosfa() {									return iosfa;							}
	public void setIosfa(String iosfa) {						this.iosfa = iosfa;						}
	
	public String getLugarDeTrabajo() {							return lugarDeTrabajo;					}
	public void setLugarDeTrabajo(String lugarDeTrabajo) {		this.lugarDeTrabajo = lugarDeTrabajo;	}
	
	public String getPathFoto() {								return pathFoto;						}
	public void setPathFoto(String pathFoto) {					this.pathFoto = pathFoto;				}
	
	public void vaciar(){
		codigo 				= 0			;
		nombre 				= ""		;
		apellido			= ""		;
		direccion			= ""		;
		
		partido				= null		;	
		telefonoParticular	= ""		;
		telefonoCelular		= ""		;	
		dni					= ""		;
		
		cuil				= ""		;
		codigoDeBarras		= ""		;
		fechaDeNacimiento	= null		;
		email				= ""		;
		
		iosfa				= ""		;
		lugarDeTrabajo		= ""		;
		pathFoto			= ""		;

	}
	
	public boolean equals(Object obj){
		boolean bln=false;
		if(obj instanceof Persona){
			Persona per =(Persona)obj;
			bln=per.getCodigo()== codigo																	&&
				per.getNombre()!=null				?per.getNombre().equals(nombre)			
														:per.getNombre()			==nombre				&&
				per.getApellido()!=null				?per.getApellido().equals(apellido) 	
														:per.getApellido()			==apellido				&&
				per.getDireccion()!=null			?per.getDireccion().equals(direccion)	
														:per.getDireccion()			==direccion				&&
				per.getPartido()!=null				?per.getPartido().equals(partido)		
														:per.getPartido()			==partido				&&
				per.getTelefonoParticular()!=null 	?	per.getTelefonoParticular().equals(telefonoParticular)
														: per.getTelefonoParticular()==telefonoParticular 	&&
				per.getTelefonoCelular() !=null		?  	per.getTelefonoCelular().equals(telefonoCelular)
														: per.getTelefonoCelular()	== telefonoCelular 		&&
				per.getDni() != null 				?per.getDni().equals(dni)
														: per.getDni()				==dni					&&
				per.getCuil()!=null					?per.getCuil().equals(cuil)
														:per.getCuil()				==cuil					&&
				per.getCodigoDeBarras() !=null		?per.getCodigoDeBarras().equals(codigoDeBarras)
														:per.getCodigoDeBarras()	==codigoDeBarras		&&
				per.getFechaDeNacimiento()!=null	?per.getFechaDeNacimiento().equals(fechaDeNacimiento)
														:per.getFechaDeNacimiento()	== fechaDeNacimiento	&&
				per.getEmail()!=null				?per.getEmail().equals(email)
														:per.getEmail()				==email					&&
				per.getIosfa() != null				?per.getIosfa().equals(iosfa)
														:per.getIosfa()				==iosfa					&&
				per.getLugarDeTrabajo()!=null		?per.getLugarDeTrabajo().equals(lugarDeTrabajo)
														:per.getLugarDeTrabajo()	==lugarDeTrabajo		&&
				per.getPathFoto() != null			?per.getPathFoto().equals(pathFoto)
														:per.getPathFoto()			==pathFoto				;
		}
		return bln;
	}
	public int hashcode(){		 
		return 	codigo														+
				nombre				!=null?nombre.hashCode()			:0 	+
				apellido			!=null?apellido.hashCode()			:1 	+
				direccion			!=null?direccion.hashCode()			:2 	+
				(partido				!=null?partido.hashCode()		:3)	+
				telefonoParticular	!=null?telefonoParticular.hashCode():4	+
				telefonoCelular		!=null?telefonoCelular.hashCode()	:5	+
				dni					!=null?dni.hashCode()				:6	+
				cuil				!=null?cuil.hashCode()				:7 	+
				codigoDeBarras		!=null?codigoDeBarras.hashCode()	:8	+
				(fechaDeNacimiento	!=null?fechaDeNacimiento.hashCode()	:9)	+
				email				!=null?email.hashCode()				:10	+
				iosfa				!=null?iosfa.hashCode()				:11	+
				lugarDeTrabajo		!=null?lugarDeTrabajo.hashCode()	:12	+
				pathFoto			!=null?pathFoto.hashCode()			:13	;

	}
	
	public String toString(){
		StringBuffer sb= new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(",nombre=");
	    sb.append(nombre);
	    sb.append(",apellido=");
	    sb.append(apellido);
	    sb.append(", direccion=");
	    sb.append(direccion);
	    sb.append(",direccion=");
	    sb.append(direccion);
	    sb.append(",partido=");
	    sb.append(partido);
	    sb.append(", telefonoParticular=");
	    sb.append(telefonoParticular);
	    sb.append(", = telefonoCelular");
	    sb.append(telefonoCelular);
	    sb.append(", dni=");
	    sb.append(dni);
	    sb.append(",cuil=");
	    sb.append(cuil);
	    sb.append(", codigoDeBarras=");
	    sb.append(codigoDeBarras);
	    sb.append(", fechaDeNacimiento=");
	    sb.append(fechaDeNacimiento);
	    sb.append(", email=");
	    sb.append(email);
	    sb.append(", iosfa=");
	    sb.append(iosfa);
	    sb.append(", lugarDeTrabajo=");
	    sb.append(lugarDeTrabajo);
	    sb.append(", pathFoto=");
	    sb.append(pathFoto);
		return sb.toString();
	}
	
	

}
