package ar.com.inac.sifaa.modelo.exceptions;

public class ExcelException extends Exception {

	public ExcelException() {super();}

	public ExcelException(String message) {
		super(message);
	}
	

}
