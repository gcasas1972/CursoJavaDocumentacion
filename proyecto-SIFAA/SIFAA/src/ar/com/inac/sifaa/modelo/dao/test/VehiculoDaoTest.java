package ar.com.inac.sifaa.modelo.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.inac.sifaa.modelo.Partido;
import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.Vehiculo;
import ar.com.inac.sifaa.modelo.dao.VehiculoDao;
import ar.com.inac.sifaa.util.ConnectionManager;

public class VehiculoDaoTest {

	static private ConnectionManager connectionManager;
	static private Connection conexion;
	static private Statement  consulta ;
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(
			//		"jdbc:mysql://192.168.1.7:3306/sifaa", "sistema",					
			//		"sistema");
					"jdbc:mysql://localhost:3306/sifaa", "root",					
					"administrador");
			consulta = conexion.createStatement();

			String sql = "";
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					VehiculoDaoTest.class.getResource(
							"CrearVehiculos.sql").openStream()));
			while ((sql = bf.readLine()) != null) {
				if (sql.trim().length() != 0 && !sql.startsWith("--")) {
					//Logger.info(ProfesorDaoTest.class, sql);
					consulta.executeUpdate(sql); // aca arma
				}
			}
			consulta.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void afterClass() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sifaa", "root",
					"administrador");
			consulta = conexion.createStatement();

			String sql = "";
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					PersonaDaoTest.class.getResource(
							"EliminarVehiculos.sql").openStream()));
			while ((sql = bf.readLine()) != null) {
				if (sql.trim().length() != 0 && !sql.startsWith("--")) {
					//Logger.info(ProfesorDaoTest.class, sql);
					consulta.executeUpdate(sql); // aca arma
				}
			}
			consulta.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	@Test
	public void testAgregar() throws HibernateException, Exception {
		
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();
		
		Vehiculo veh = new Vehiculo("NuevoMarca_test", "NuevoModelo_test", "NuevoColor_test", "NuevoDominio_test");
		veh.setPersona(new Persona(1));
		
		VehiculoDao vehDao = new VehiculoDao();
		vehDao.agregarModificar(veh, connectionManager.getSession());
		
		connectionManager.commitTx()	;
		connectionManager.desconectar()	;
		
	}

	@SuppressWarnings("unchecked")
	@Test
	
	public void testModificar() throws HibernateException, Exception {
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();
		
		VehiculoDao vehDao = new VehiculoDao();
		Vehiculo vehLeida = (Vehiculo)(vehDao.leer(new Vehiculo(0, null, "Gabriel1_test", null, null, null), connectionManager.getSession()).iterator().next());
		
		vehLeida.setMarca("Volkswagen1_Modificado_test");
		vehLeida.setModelo("Gol1_Modificado_test");
		
		vehDao.agregarModificar(vehLeida, connectionManager.getSession());
		
		connectionManager.commitTx()	;
		connectionManager.desconectar()	;
		
	}
	
	public void testEliminar() {
		fail("Not yet implemented");
	}

	@Test
	public void testLeer() {
		fail("Not yet implemented");
	}

}
