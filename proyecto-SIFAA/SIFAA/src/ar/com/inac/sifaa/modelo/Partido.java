package ar.com.inac.sifaa.modelo;

public class Partido {
	private int 	codigo		;
	private String 	descripcion	;
	
	//constructores
	public Partido(){
		codigo = -1;
	}
	public Partido(String pDesc) {
		codigo = -1;
		descripcion = pDesc;
	}
	
	public Partido(int pCod, String pDesc) {
		codigo = pCod;
		descripcion = pDesc;
	}
	public int getCodigo() {			return codigo;			}
	public void setCodigo(int codigo) {	this.codigo = codigo;	}
	
	public String getDescripcion() {					return descripcion;				}	
	public void setDescripcion(String descripción) {	this.descripcion = descripción;	}
	
	public boolean equals(Object obj){
		boolean bln=false;
		if(obj instanceof Partido){
			Partido part =(Partido) obj;
			bln = 	part.getCodigo()  == codigo && 
					part.getDescripcion()!=null &&
					part.getDescripcion().equals(descripcion);
		}
			
			
		return bln;
	}
	
	public int hashCode(){
		return 	codigo + 
				descripcion!=null?descripcion.hashCode():0;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(",descripcion=");
		sb.append(descripcion);
		return sb.toString();
	}

}
