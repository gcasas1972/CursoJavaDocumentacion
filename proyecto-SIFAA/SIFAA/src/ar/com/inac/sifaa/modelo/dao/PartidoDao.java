package ar.com.inac.sifaa.modelo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.com.inac.sifaa.modelo.Partido;

public class PartidoDao implements DAO {

	public Object agregarModificar(Object obj, Session session) throws Exception {
		
		if (session!=null){
			try{
				obj=session.merge(obj);//pq no merge???!!!MERGE me devuelve el obj actualizado
				System.out.println("obj=" + obj);
				return obj;
			} catch (Exception e) {// SAVE devuelve el id serializable
				e.printStackTrace();
				throw e;//PERSIST no devuelve nada @persist(String entityName,Object object)
			}
		}else return false ;
	}

	public Object eliminar(Object obj, Session session) throws Exception {
		if (session!=null){			
			try {
				session.delete(obj);
				return obj;
				//getSession().flush();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}return false;
	}

	public List leer(Object obj, Session session) throws Exception {
		List l = null;
		
		if (session!=null){
			if( !vacio((Partido)obj)){
				Partido partido	= 	null;
				
					partido= (Partido)obj;
					Criteria criteria=session.createCriteria(Partido.class);
					if (partido.getCodigo()>0)
						criteria.add(Restrictions.eq("codigo", partido.getCodigo()));					
					if (partido.getDescripcion()!= null && !partido.getDescripcion().isEmpty() )
						criteria.add(Restrictions.ilike("descripcion", partido.getDescripcion()+ "%"));					
					l=criteria.list();
			}			
			else{
				Criteria criteria=session.createCriteria(Partido.class);
				l=criteria.list();
				//l=session.createQuery("from partido").list();	}
		}
		}
		
		return l;	
	
	}
	
	private boolean vacio(Partido partido){		
		return 	partido	==	null 			||
				partido.getCodigo()	 		== 0  &&
				( partido.getDescripcion()== null ||
				  partido.getDescripcion().isEmpty());	 
			
	}
	

}
