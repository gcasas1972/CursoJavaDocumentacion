--insert into `sifaa`.`vehiculos`( `PER_ID`,`VEH_MARCA`,`VEH_MODELO`,`VEH_COLOR`,`VEH_DOMINIO`)values (1, 'Volkswagen1_test', 'Gol1_test', 'Blanco1_test', 'GGE9441_test')
--insert into `sifaa`.`vehiculos`( `PER_ID`,`VEH_MARCA`,`VEH_MODELO`,`VEH_COLOR`,`VEH_DOMINIO`)values (2, 'Volkswagen2_test', 'Gol2_test', 'Blanco2_test', 'GGE9442_test')
--insert into `sifaa`.`vehiculos`( `PER_ID`,`VEH_MARCA`,`VEH_MODELO`,`VEH_COLOR`,`VEH_DOMINIO`)values (3, 'Volkswagen3_test', 'Gol3_test', 'Blanco3_test', 'GGE9443_test')
--insert into `sifaa`.`vehiculos`( `PER_ID`,`VEH_MARCA`,`VEH_MODELO`,`VEH_COLOR`,`VEH_DOMINIO`)values (4, 'Volkswagen4_test', 'Gol4_test', 'Blanco4_test', 'GGE9444_test')


-- primer insertar una persona

insert into `sifaa`.`personas`
( `PART_ID`         ,`PER_NOMBRE` ,`PER_APELLIDO`      ,`PER_DIRECCION`     ,`PER_TELEFONOPART`,
  `PER_TELEFONOCEL` ,`PER_DNI`    ,`PER_CUIL`          ,`PER_CODIGODEBARRAS`,`PER_FECHADENACIMIENTO`,
  `PER_EMAIL`       ,`PER_IOSFA`  ,`PER_LUGARDETRABAJO`,`PER_PATHFOTO`)
  values (1         ,'Gabriel1_test', 'Casas1_test'    , 'Solari3866_test'  , '4697-1533'            ,
  '15-6005-7247'    , '22965726'    ,'20-22965726-8'    , '1234567890'      , STR_TO_DATE('17,11,1972','%d,%m,%Y'),
  'gcasas1972@gmail.com', '123456'  , 'aulas'           , 'c:/fotos')

-- 2do agrego un vehiculo con esa  per_id

insert into `sifaa`.`vehiculos`  (PER_ID)
select per_id from personas where per_nombre like '%_test'

-- 3ro un update en la tabla vehiculos
update vehiculos
set
 VEH_MARCA='marca_test',
 VEH_MODELO='modelo_test',
 VEH_COLOR ='color_test',
 VEH_DOMINIO='dominio_test'
where per_id =(select per_id from personas where per_nombre like '%_test')

-- 
