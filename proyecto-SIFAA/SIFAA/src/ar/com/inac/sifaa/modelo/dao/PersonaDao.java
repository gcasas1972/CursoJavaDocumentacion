package ar.com.inac.sifaa.modelo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ar.com.inac.sifaa.modelo.Persona;

public class PersonaDao implements DAO {

	public Object agregarModificar(Object obj, Session session) throws Exception {
		
		if (session!=null){
			try{
				obj=session.merge(obj);//pq no merge???!!!MERGE me devuelve el obj actualizado
				System.out.println("obj=" + obj);
				return obj;
			} catch (Exception e) {// SAVE devuelve el id serializable
				e.printStackTrace();
				throw e;//PERSIST no devuelve nada @persist(String entityName,Object object)
			}
		}else return false ;
	}

	public Object eliminar(Object obj, Session session) throws Exception {
		if (session!=null){			
			try {
				session.delete(obj);
				return obj;
				//getSession().flush();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}return false;
	}

	public List leer(Object obj, Session session) throws Exception {
		List l = null;
		
		if (session!=null){
			if( !vacio((Persona)obj)){
				Persona persona	= 	null;
				
					persona= (Persona)obj;
					Criteria criteria=session.createCriteria(Persona.class);
					if (persona.getCodigo()>0)
						criteria.add(Restrictions.eq("codigo", persona.getCodigo()));
					if (persona.getDni()!=null && !persona.getDni().isEmpty())
						criteria.add(Restrictions.eq("dni", persona.getDni()));					

					if (persona.getNombre()!= null && !persona.getNombre().isEmpty() )
						criteria.add(Restrictions.ilike("nombre", persona.getNombre()+ "%"));					
					if (persona.getApellido()!= null && !persona.getApellido().isEmpty() )
						criteria.add(Restrictions.ilike("apellido", persona.getApellido()+ "%"));
					//TODO ALGUIEN debe agregar toooodos los campos
//					session.refresh(iniciador);
					l=criteria.list();
			}			
			else{	l=session.createQuery("from Persona").list();	}
		}
		return l;	
	}
	private boolean vacio(Persona persona){		
		return 	persona					==	null 	||
				persona.getCodigo()		<  1		&&
				persona.getApellido()	== null		&&
				persona.getNombre()		== null;	 
			
	}
	

}
