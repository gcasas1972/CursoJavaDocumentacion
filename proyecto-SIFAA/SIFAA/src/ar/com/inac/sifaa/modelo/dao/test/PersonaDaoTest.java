package ar.com.inac.sifaa.modelo.dao.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.HibernateException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.inac.sifaa.modelo.Partido;
import ar.com.inac.sifaa.modelo.Persona;
import ar.com.inac.sifaa.modelo.dao.PersonaDao;
import ar.com.inac.sifaa.util.ConnectionManager;


/**
 * Test unitario para operaciones CRUD
 * con objeto {@link Iniciador}.
 * 
 * @author Santos Sanchez
 */
public class PersonaDaoTest {
	static private ConnectionManager connectionManager;
	static private Connection conexion;
	static private Statement  consulta ;	
	

	@BeforeClass
	public static void beforeClass() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sifaa", "sistema",					
					"sistema");
			consulta = conexion.createStatement();

			String sql = "";
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					PersonaDaoTest.class.getResource(
							"CrearPersonas.sql").openStream()));
			while ((sql = bf.readLine()) != null) {
				if (sql.trim().length() != 0 && !sql.startsWith("--")) {
					//Logger.info(ProfesorDaoTest.class, sql);
					consulta.executeUpdate(sql); // aca arma
				}
			}
			consulta.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void afterClass() throws ClassNotFoundException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sifaa", "sistema",
					"sistema");
			consulta = conexion.createStatement();

			String sql = "";
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					PersonaDaoTest.class.getResource(
							"EliminarPersonas.sql").openStream()));
			while ((sql = bf.readLine()) != null) {
				if (sql.trim().length() != 0 && !sql.startsWith("--")) {
					//Logger.info(ProfesorDaoTest.class, sql);
					consulta.executeUpdate(sql); // aca arma
				}
			}
			consulta.close();
			conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Test para lectura de un iniciador
	 * previamente insertado en lote sql.
	 */
	@Test
	public void testAgregar() throws HibernateException, Exception {
 
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();
		
		Persona per = new Persona("Nuevo Gabrielito_test", "Nuevo Casas_test");
		per.setPartido(new Partido(86,"Morón"));
		
		
		
		PersonaDao profDao = new PersonaDao();
		profDao.agregarModificar(per,connectionManager.getSession());
		
		connectionManager.commitTx();
		connectionManager.desconectar();
		
		
	}
	
	/**
	 * Test para modificación de un iniciador
	 * previamente insertado en lote sql.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testModificar() throws HibernateException, Exception {
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();
		
		PersonaDao perDao= new PersonaDao();
		Persona perLeida = (Persona)(perDao.leer(new Persona("Gabriel1_test"), connectionManager.getSession())).iterator().next();
		
		perLeida.setNombre("Gabriel1_Modificado_test");
		perLeida.setApellido("Casas1 modificado_test");
		
		perDao.agregarModificar(perLeida, connectionManager.getSession());
		
		
		connectionManager.commitTx();
		connectionManager.desconectar();		
	}
	
	/**
	 * Test para eliminación de un iniciador
	 * previamente insertado en lote sql.
	 */
	@Test
	public void testEliminar() throws HibernateException, Exception {
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();

		PersonaDao perDao= new PersonaDao();
		Persona perLeida = (Persona)(perDao.leer(new Persona("Gabriel3_test"), connectionManager.getSession())).iterator().next();
		perDao.eliminar(perLeida, connectionManager.getSession());
		
		connectionManager.commitTx();
		connectionManager.desconectar();		
	}	
	
	/**
	 * Crea una conexión JDBC a la DB y devuelve un Statement
	 * para realizar consultas a la misma.
	 * 
	 * @return Un {@link Statement}
	 * @throws Exception
	 */
	@Test
	public  void test_leerProfesor() throws Exception {
		connectionManager = new ConnectionManager();
		connectionManager.conectar();
		connectionManager.beginTx();
				
		
		// consulta de iniciadores
//		PersonaDao profDao = new PersonaDao();
//		@SuppressWarnings("unchecked")
//		List<Persona> profesores = profDao.leer(new Persona("Gabriel2_test"), connectionManager.getSession());
//		
//		Assert.assertEquals(profesores.get(0), new Persona("Gabriel1_test", "Casas1_test"));
//
//		// comprobaciones6
		
		
		connectionManager.conectar();
		connectionManager.beginTx();
				
	}		
	

	}
