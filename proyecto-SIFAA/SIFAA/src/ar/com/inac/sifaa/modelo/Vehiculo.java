package ar.com.inac.sifaa.modelo;

public class Vehiculo {
		private int codigo		;
		private Persona persona ;
		private String marca	;
		private String modelo	;
		
		private String color	;
		private String dominio	;
		private Persona nombre;
		
		//constructores
		public Vehiculo(String pMar, String pMod, String pCol, String pDom) {
			codigo = -1			;
			marca = pMar		;
			modelo = pMod		;
			color = pCol		;
			dominio = pDom		;
		}
		
		public Vehiculo(int codigo, Persona persona, String marca, String modelo, String color, String dominio){
			super()				;
			this.codigo = codigo	;
			this.persona = persona	;
			this.marca = marca		;
			this.modelo = modelo	;
			this.color = color		;
			this.dominio = dominio	;
		}
		
		public Vehiculo(int pCod){
			codigo = pCod			; 
		}
		
		public int getCodigo() {				return codigo;			}
		public void setCodigo(int codigo) {		this.codigo = codigo;	}
		
		public Persona getPersona() {			return persona;			}
		public void setPersona(Persona nombre){ this.nombre = nombre; }
				
		public String getMarca() {				return marca;			}
		public void setMarca(String marca) {	this.marca = marca;		}
		
		public String getModelo() {				return modelo;			}
		public void setModelo(String modelo) {this.modelo = modelo;		}
		
		public String getColor() {				return color;			}
		public void setColor(String color) {	this.color = color;		}
		
		public String getDominio() {			return dominio;			}
		public void setDominio(String dominio) {this.dominio = dominio;	}
		
		public void vaciar(){
			codigo = 0;
			persona = null;
			marca = "" ;
			modelo = "";
			color = "";
			dominio = "";
		}
		
		
		public boolean equals(Object obj){
			boolean bln = false;
			if (obj instanceof Vehiculo){
				Vehiculo veh = (Vehiculo)obj;
				
				bln = 	veh.getCodigo()== codigo &&
						veh.getMarca() != null 	?veh.getMarca().equals(marca) 	: veh.getMarca() == marca 	&&
						veh.getModelo() != null ?veh.getModelo().equals(modelo) : veh.getModelo() == modelo &&
						veh.getColor() != null	?veh.getColor().equals(color) 	: veh.getColor() == color  	&&
						veh.getDominio() != null ?veh.getDominio().equals(dominio) : veh.getDominio() == dominio ;
			}
			return bln;
		}
		
		public int hashCode(){
			return 	codigo						+
					marca 	!= null		?marca.hashCode() 		:0 +
					modelo	!= null		?modelo.hashCode()		:1 +
					color	!= null		?color.hashCode()		:2 +
					dominio != null 	?dominio.hashCode() 	:3;
		}
		
		public String toString(){
			StringBuffer sb = new StringBuffer("codigo=");
			sb.append(codigo);
			sb.append(",marca=");
			sb.append(marca);
			sb.append(",modelo=");
			sb.append(modelo);
			sb.append(", color=");
			sb.append(color);
			sb.append(",dominio=");
			sb.append(dominio);
			return sb.toString();
		}
}
